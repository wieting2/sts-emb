f = open('datasets_tokenized2/SNLI-train','r')
lines = f.readlines()
lines.pop(0)

snli = set([])
for i in lines:
    i = i.split('\t')
    ln = i[0] + i[1]
    ln = ln.replace(' ','').strip().lower()
    snli.add(ln)
    ln = i[1] + i[0]
    ln = ln.replace(' ','').strip().lower()
    snli.add(ln)

f = open('datasets_tokenized2/sicktest-ent','r')
lines = f.readlines()

sick = set([])
for i in lines:
    i = i.split('\t')
    ln = i[0] + i[1]
    ln = ln.replace(' ','').strip().lower()
    sick.add(ln)
    ln = i[1] + i[0]
    ln = ln.replace(' ','').strip().lower()
    sick.add(ln)

sick = list(sick)

for i in sick:
    if i in snli:
        print i
