
import sys
import score_using_model
from collections import defaultdict
import pdb

def trigram_overlap(pairs1, pairs2):
    def get_trigram_overlap_score(t, r):
        temp = []
        for i in range(len(t) - 2):
            temp.append(t[i] + " " + t[i + 1] + " " + t[i + 2])
        t = temp
        temp = []
        for i in range(len(r) - 2):
            temp.append(r[i] + " " + r[i + 1] + " " + r[i + 2])
        r = temp
        start = list(start)
        den = len(start)
        if den == 0:
            return 0.
        num = 0
        for i in range(len(start)):
            if start[i] in end:
                num += 1
        return float(num) / den
    scores = []
    for i in range(len(pairs1)):
        scores.append(get_trigram_overlap_score(pairs1[i], pairs2[i]))
    return scores

def get_trigram_overlap2(t,r):
    temp = []
    for i in range(len(t) - 2):
        temp.append(t[i] + " " + t[i + 1] + " " + t[i + 2])
    t = temp
    temp = []
    for i in range(len(r) - 2):
        temp.append(r[i] + " " + r[i + 1] + " " + r[i + 2])
    r = temp
    start = list(start)
    den = len(start)
    if den == 0:
        return 0.
    num = 0
    for i in range(len(start)):
        if start[i] in end:
            num += 1
    return float(num) / den

f = open(sys.argv[1],'r')
lines = f.readlines()

para = defaultdict(list)

lines.pop(0)

for i in lines:
    l = i.split('\t')
    idx = l[0]+l[1]
    para[idx].append(i)

for i in para:
    lis1, lis2 = [], []
    for j in range(1,len(para[i])):
        gold = para[i][0].split('\t')[-1]
        pi = para[i][j].split('\t')[-1]
        lis1.append(gold)
        lis2.append(pi)
    scores = score_using_model.score_pairs(lis1, lis2)
    ovl = trigram_overlap(lis1, lis2)
    pdb.set_trace()
