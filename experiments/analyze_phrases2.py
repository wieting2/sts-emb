from glob import glob

class dataset(object):
    def __init__(self, d):
        d = d.strip().split()
        self.name = d[-1]
        self.p = float(d[0])
        if len(d) == 3:
            self.s = float(d[1])

class evaluation(object):
    def __init__(self,line):
        self.result = line
        line = line.split("|")
        self.datasets = {}
        for i in line:
            if len(i.split()) > 0:
                d = dataset(i)
                self.datasets[d.name]=d

    def getTunedResults(self):
        return self.datasets['JHUppdb'].s
    
    def printResults(self):
        print self.result


bd = 0
flist = glob('slurm*.out')
dd = {}
for i in flist:
    f = open(i,'r')
    lines = f.readlines()
    best = 0.
    key = None
    for j in lines:
        if 'train' in j:
            name = j
        if 'total |' in j:
            t = float(j.split()[0])
            if t > best:
                best = t
                dd[name] = best

print len(dd)
print len(flist)
dd_sorted=sorted(dd.items(), key=lambda x: x[1], reverse=True)

for i in dd_sorted:
    print i
