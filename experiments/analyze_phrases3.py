from glob import glob

class dataset(object):
    def __init__(self, d):
        d = d.strip().split()
        self.name = d[-1]
        self.p = float(d[0])
        if len(d) == 3:
            self.s = float(d[1])

class evaluation(object):
    def __init__(self,line):
        self.result = line
        line = line.split("|")
        self.datasets = {}
        for i in line:
            if len(i.split()) > 0:
                d = dataset(i)
                self.datasets[d.name]=d

    def getTunedResults(self):
        return self.datasets['2016-average'].p
    
    def printResults(self):
        print self.result


bd = 0
flist = glob('slurm*.out')
dd = {}
for i in flist:
    f = open(i,'r')
    lines = f.readlines()
    best = 0.
    key = None
    for n,j in enumerate(lines):
        if 'train' in j:
            name = j
        if 'JHUppdb' in j:
            e = evaluation(j)
            t = e.getTunedResults()
            if t > best:
                best = t
                dd[name] = (best,e.result,lines[n+1].split()[0])

print len(dd)
print len(flist)
dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)

for i in dd_sorted:
    print i[0],i[1][2].strip(),i[1][1]