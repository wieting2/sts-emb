import sys

margin = [0.4]
mb = [40]
lw = [0]
dataf = ['../long_data_preprocessed.txt']

nns = ["gran", "granmax", "lstmavg", "lstmmax", "wordaverage", "maxpool", "bilstmavg", "bilstmmax"]

cmd = "sh train.sh train_nmt6.py -epochs 1 -save false -num_examples 1000000 -batchsize 100 -data2 True"

for m in margin:
    for b in mb:
        for c in lw:
            for d in dataf:
                for n in nns:
                    if n not in ["bilstmavg", "bilstmmax"]:
                        print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 -wordtype words " \
                                        "-dataf ../{3} -nntype {4}".format(m, b, c, d, n)
                    if n in ["bilstmavg", "bilstmmax"]:
                        print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 -wordtype words " \
                                    "-dataf ../{3} -nntype {4} -axis 1".format(m, b, c, d, n)
                        print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 150 -wordtype words " \
                                    "-dataf ../{3} -nntype {4} -axis 2".format(m, b, c, d, n)
                    if n not in ["wordaverage", "maxpool"]:
                        if n in ["bilstmavg", "bilstmmax"]:
                            print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 -wordtype words " \
                                    "-dataf ../{3} -nntype {4} -scramble 0.3 -axis 1".format(m, b, c, d, n)
                            print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 150 -wordtype words " \
                                    "-dataf ../{3} -nntype {4} -scramble 0.3 -axis 2".format(m, b, c, d, n)
                        else:
                            print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 -wordtype words " \
                                    "-dataf ../{3} -nntype {4} -scramble 0.3".format(m, b, c, d, n)
