import sys

margin = [0.4]
mb = [1,20,40,60]
lw = [0]
dataf = ['../long_data_preprocessed.txt']

nns = ["lstmavg"]

cmd = "sh train.sh train_nmt6.py -epochs 1 -save true -num_examples 5000000 -batchsize 100 -data2 True"

for m in margin:
    for b in mb:
        for c in lw:
            for d in dataf:
                for n in nns:
                    print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 -wordtype words " \
                                        "-dataf ../{3} -nntype {4} -outfile lstmavg-{1}".format(m, b, c, d, n)
