import sys

margin = [0.4]
mb = [40]
lw = [0]
dataf = ['../long_data_preprocessed.txt']

nns = ["gran", "granmax", "lstmavg", "lstmmax", "wordaverage", "maxpool", "bilstmavg", "bilstmmax"]

cmd = "sh train.sh train_nmt6.py -epochs 5 -save true -num_examples 5000000 -batchsize 100 -data2 True"

for m in margin:
    for b in mb:
        for c in lw:
            for d in dataf:
                for n in nns:
                    if n in ["bilstmavg", "bilstmmax", "lstmavg", "lstmmax"]:
                        print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 900 -wordtype words " \
                                    "-dataf ../{3} -nntype {4} -axis 1 -outfile {4}-1".format(m, b, c, d, n)
                        print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 450 -wordtype words " \
                                    "-dataf ../{3} -nntype {4} -axis 2 -outfile {4}-2".format(m, b, c, d, n)

