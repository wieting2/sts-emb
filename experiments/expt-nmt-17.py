import sys

margin = [0.4]
mb = [40]
lw = [0]
dataf = ['../long_data_preprocessed.txt']
combotype = ["ngram-word", "ngram-lstm", "ngram-word-lstm", "word-lstm"]

cmd = "sh train.sh train_nmt5.py -epochs 5 -save false -num_examples 5000000 -batchsize 100 -save true -data2 True"

#combination models
for m in margin:
    for b in mb:
        for c in lw:
            for d in dataf:
                for comb in combotype:
                    print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 " \
                            "-dataf ../{3} -combine add -combo_type {4} -outfile {5}".format(m, b, c, d, comb,
                                                                                             "{0}-{1}".format(comb, b))
                    print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 " \
                            "-dataf ../{3} -combine concat -combo_type {4} -outfile {5}".format(m, b, c, d, comb,
                                                                                                "{0}-{1}".format(comb, b))

print ""
cmd = "sh train.sh train_nmt6.py -epochs 5 -save true -num_examples 5000000 -batchsize 100 -data2 True"

#single models
for m in mb:
    print cmd + ' -nntype wordaverage ' \
      '-batchsize 100 -margin 0.4 -mb_batchsize {0} -LW 0 -dim 300 -wordtype 3grams ' \
      '-dataf ../../long_data_preprocessed.txt -outfile {1}'.format(m, "3gramsavg-{0}".format(m))
    print cmd + ' -nntype wordaverage ' \
      '-batchsize 100 -margin 0.4 -mb_batchsize {0} -LW 0 -dim 300 -wordtype words ' \
      '-dataf ../../long_data_preprocessed.txt -outfile {1}'.format(m, "wordavg-{0}".format(m))
    print cmd + ' -nntype lstmavg ' \
      '-batchsize 100 -margin 0.4 -mb_batchsize {0} -LW 0 -dim 300 -wordtype words ' \
      '-dataf ../../long_data_preprocessed.txt -scramble 0.3 -outfile {1}'.format(m, "lstmavg-{0}".format(m))
