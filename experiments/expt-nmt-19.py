import sys
from glob import glob

margin = [0.4]
mb = [40]
lw = [0]
dataf = glob('../../*ws_sorted*') + glob('../../*scorer_sorted*')  + glob('../../*trans_sorted*')  + glob('../../*tri_sorted*')

nns = ["lstmavg", "wordaverage"]

cmd = "sh train.sh train_nmt6.py -epochs 1 -save false -num_examples 1000000 -batchsize 100 -data2 False"

for m in margin:
    for b in mb:
        for c in lw:
            for d in dataf:
                for n in nns:
                    if "wordaverage" in n:
                        print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 -wordtype trigrams " \
                                    "-dataf {3} -nntype {4} -axis 1 -outfile {4}-1".format(m, b, c, d, n)
                    print cmd + " -margin {0} -mb_batchsize {1} -LW {2} -dim 300 -wordtype words " \
                        "-dataf {3} -nntype {4} -axis 1 -outfile {4}-1".format(m, b, c, d, n)
