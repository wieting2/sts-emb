import sys

if len(sys.argv) > 1:
    prefix = "../../"
else:
    prefix = "../data/"

margin = [0.4,0.6,0.8]
batchsize = [25,50,100]
#cutoff = [0,10,100]
lw = [1E-4,1E-5,1E-6,1E-7,1E-8,0]

vocab_files = [
"books-800-vocab.txt",
"enwiki-800-vocab.txt",
"news-800-vocab.txt",
"tweets-800-vocab.txt"
]

f = open('emb_list.txt','r')
lines = f.readlines()
emb_files = []
for i in lines:
    if len(i) > 0:
        i = i.strip()
        emb_files.append(i)

cmd = "sh train_word.sh train.py -epochs 10"

e = "enwiki-15-w2v.sg.txt"
v = "enwiki-800-vocab.txt"

for m in margin:
    for b in batchsize:
        for c in lw:
            print cmd + " -margin {0} -batchsize {1} -LW {2} -dim 300 -wordfile {5}{3} -vocabfile {5}{4}".format(m, b, c, e, v, prefix)
