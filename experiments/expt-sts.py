import sys
from glob import glob

margin = [0.4]
mb = [40]
lw = [0]

nns = ["bilstmavg", "bilstmmax"]

cmd = "sh train2.sh train_nmt6.py -epochs 15 -save true -batchsize 100"

for c in lw:
    for n in nns:
        print cmd + " -LW {0} -dim 2048 -wordtype words -axis 2" \
            " -nntype {1} -outfile {1}-2".format(c, n)
        print cmd + " -LW {0} -dim 2048 -wordtype words -axis 1" \
            " -nntype {1} -outfile {1}-1".format(c, n)