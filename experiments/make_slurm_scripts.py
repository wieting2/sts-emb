f = open('cmds.txt','r')
lines = f.readlines()

ct = 0
cmds = []
while ct < len(lines):
    if ct % 5000 == 0 and ct > 0:
        idx = ct / 1000
        fout = open('batch-cmds-word-'+str(idx)+'.txt','w')
        for i in cmds:
            fout.write(i.strip()+'\n')
        fout.close()
        fname = 'slurm-script-'+str(idx)+'.txt'
        fout = open(fname,'w')
        fout.write('#!/bin/bash\n')
        fout.write('#SBATCH --partition=contrib-gpu --cpus-per-task=1 --array=1-'+str(len(cmds))+'\n')
        fout.write('bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" '+'batch-cmds-word-'+str(idx)+'.txt'+'`"')
        fout.close()
        cmds = []
    cmds.append(lines[ct])
    ct += 1

fout = open('batch-cmds-word-last.txt','w')
for i in cmds:
    fout.write(i.strip()+'\n')
fout.close()
fname = 'slurm-script-word-last.txt'
fout = open(fname,'w')
fout.write('#!/bin/bash\n')
fout.write('#SBATCH --partition=speech-gpu --cpus-per-task=1 --array=1-'+str(len(cmds))+'\n')
fout.write('bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" '+'batch-cmds-word-last.txt'+'`"')
fout.close()
