f1 = '../data/simple.aligned'
f2 = '../data/normal.aligned'

def get_lines(f):
    f = open(f,'r')
    lines = f.readlines()
    d = []
    for i in lines:
        i = i.split('\t')
        d.append(i[-1].strip().lower())
    return d

data = []

d1 = get_lines(f1)
d2 = get_lines(f2)

for i in range(len(d1)):
    data.append((d1[i],d2[i]))

def get_words(examples):
    d = {}
    for i in examples:
        e1 = i[0].split()
        e2 = i[1].split()
        for j in range(len(e1)):
            wd = e1[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
        for j in range(len(e2)):
            wd = e2[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
    return d

d = get_words(data)
d = sorted(d.items(), key=lambda x : x[1])
#for i in d:
    #print i[0].strip() + "\t" + str(i[1])

#for i in data:
#    print i[0]
#    print i[1]

for i in data:
    print i[0].replace(" ","_").lower()
    print i[1].replace(" ","_").lower()