
import lasagne

class lasagne_add_layer(lasagne.layers.MergeLayer):
    
    def __init__(self, incoming, tosum = False, **kwargs):
        super(lasagne_add_layer, self).__init__(incoming, **kwargs)

    def get_output_for(self, inputs, **kwargs):
        emb1 = inputs[0]
        emb2 = inputs[1]
        if len(inputs) == 3:
            emb3 = inputs[2]
            return emb1 + emb2 + emb3
        return emb1 + emb2

    def get_output_shape_for(self, input_shape):
        #print input_shape
        return input_shape