from bag_nmt_model3 import bag_nmt_model
from tree2 import tree
import argparse
import cPickle
import lasagne
import io
import nltk
from copy import deepcopy

parser = argparse.ArgumentParser()
parser.add_argument("-infile")
args = parser.parse_args()

args.margin = 0.4
args.LW = 0
args.learner = lasagne.updates.adam
args.eta = 0.001
args.dim = 300
args.combo_type = 'ngram-word'
args.combine = 'concat'
args.model = 'ngram-word-concat-40.pickle'

params = cPickle.load(open(args.model, 'rb'))
ngram_words, wd_words = params.pop(-1)

model = bag_nmt_model(params[0], params[1], args)

def score_batch(batch, ngram_words, wd_words):
    def get_embeddings(np1, np2, ngram_words, wd_words):
        wp1 = deepcopy(np1)
        wp2 = deepcopy(np2)
        np1.populate_embeddings_ngrams(ngram_words, 3, True)
        np2.populate_embeddings_ngrams(ngram_words, 3, True)
        wp1.populate_embeddings(wd_words, True)
        wp2.populate_embeddings(wd_words, True)
        return np1.embeddings, wp1.embeddings, np2.embeddings, wp2.embeddings

    seq1n = []
    seq1w = []
    seq2n = []
    seq2w = []
    for i in batch:
        tr1, tr2 = i
        nX1, wX1, nX2, wX2 = get_embeddings(tr1, tr2, ngram_words, wd_words)
        seq1n.append(nX1)
        seq1w.append(wX1)
        seq2n.append(nX2)
        seq2w.append(wX2)

    nx1, nm1 = model.prepare_data(seq1n)
    wx1, wm1 = model.prepare_data(seq1w)
    nx2, nm2 = model.prepare_data(seq2n)
    wx2, wm2 = model.prepare_data(seq2w)

    return model.scoring_function(nx1, nm1, wx1, wm1, nx2, nm2, wx2, wm2)

#read in file and score each pair, print pair with score
with io.open(args.infile, 'r', encoding='utf-8') as f:
    batch = []
    keys = []
    for i in f:
        ln = i.strip()
        arr = i.split('\t')
        g = arr[0].strip()
        tr = arr[1].strip()
        key = arr[0].strip() + "\t" + arr[1].strip()
        gtoks = nltk.word_tokenize(g)
        gtr = nltk.word_tokenize(tr)
        tr1 = tree(" ".join(gtoks))
        tr2 = tree(" ".join(gtr))
        batch.append((tr1, tr2))
        keys.append(key)
        if len(batch) == 100:
            scores = score_batch(batch, ngram_words, wd_words)
            for n,i in enumerate(keys):
                st = i + "\t{0}\n".format(scores[n])
                print st.strip()
            batch = []
            keys = []
    if len(batch) > 0:
        scores = score_batch(batch, ngram_words, wd_words)
        for n, i in enumerate(keys):
            st = i + "\t{0}\n".format(scores[n])
            print st.strip()

