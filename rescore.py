import cPickle
import io
import sys
from copy import deepcopy

import argparse
import lasagne
import nltk
import numpy as np
from scipy.spatial.distance import cosine

code_path = '../to_release2/sts-emb-expt18/sentence_code/'

sys.path.insert(0, code_path)
from to_release.main.mixed_models import mixed_models
from tree2 import tree

parser = argparse.ArgumentParser()
parser.add_argument("-combo_type", default='ngram-word')
parser.add_argument("-combine", default='concat')
parser.add_argument("-model", default='../to_release2/sts-emb-expt20/sentence_code/ngram-word-concat-40.pickle')
parser.add_argument("-filter")
parser.add_argument("-outfile")
parser.add_argument("-dim", type = int, default=300)
args = parser.parse_args()

args.margin = 0.4
args.LW = 0
args.learner = lasagne.updates.adam
args.eta = 0.001

params = cPickle.load(open(args.model, 'rb'))
ngram_words, wd_words = params.pop(-1)

if args.combo_type == "ngram-word":
    model = mixed_models(params[0], params[1], args)
elif args.combo_type == "ngram-word-lstm":
    model = mixed_models(params[0], params[1], args, We_initial_lstm = params[2])
    lasagne.layers.set_all_param_values(model.final_layer, params)

def get_overlap(t, r, type):
    if type == 2:
        temp = []
        for i in range(len(t) - 1):
            temp.append(t[i] + " " + t[i + 1])
        t = temp
        temp = []
        for i in range(len(r) - 1):
            temp.append(r[i] + " " + r[i + 1])
        r = temp
        # t = set(t)
        # r = set(r)
    elif type == 3:
        temp = []
        for i in range(len(t) - 2):
            temp.append(t[i] + " " + t[i + 1] + " " + t[i + 2])
        t = temp
        temp = []
        for i in range(len(r) - 2):
            temp.append(r[i] + " " + r[i + 1] + " " + r[i + 2])
        r = temp
    if len(r) < len(t):
        start = r
        end = t
    else:
        start = t
        end = r
    start = list(start)
    den = len(start)
    if den == 0:
        return 0.
    num = 0
    for i in range(len(start)):
        if start[i] in end:
            num += 1
    return float(num) / den

def score_batch(batch, ngram_words, wd_words):
    def get_embeddings(np1, np2, ngram_words, wd_words):
        wp1 = deepcopy(np1)
        wp2 = deepcopy(np2)
        np1.populate_embeddings_ngrams(ngram_words, 3, True)
        np2.populate_embeddings_ngrams(ngram_words, 3, True)
        wp1.populate_embeddings(wd_words, True)
        wp2.populate_embeddings(wd_words, True)
        return np1.embeddings, wp1.embeddings, np2.embeddings, wp2.embeddings

    seq1n = []
    seq1w = []
    seq2n = []
    seq2w = []
    for i in batch:
        tr1, tr2 = i
        nX1, wX1, nX2, wX2 = get_embeddings(tr1, tr2, ngram_words, wd_words)
        seq1n.append(nX1)
        seq1w.append(wX1)
        seq2n.append(nX2)
        seq2w.append(wX2)

    nx1, nm1 = model.prepare_data(seq1n)
    wx1, wm1 = model.prepare_data(seq1w)
    nx2, nm2 = model.prepare_data(seq2n)
    wx2, wm2 = model.prepare_data(seq2w)

    return model.scoring_function(nx1, nm1, wx1, wm1, nx2, nm2, wx2, wm2)

def getWordmap(textfile):
    words={}
    We = []
    f = io.open(textfile, 'r', encoding='utf-8')
    lines = f.readlines()
    if len(lines[0].split()) == 2:
        lines.pop(0)
    ct = 0
    for (n,i) in enumerate(lines):
        i=i.split()
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=ct
        ct += 1
        We.append(v)
    return (words, np.array(We))

def lookup(w):
    w = w.lower()
    if w in words:
        return words[w]
    else:
        #print w
        return words['UUUNKKK']

words, We = getWordmap('../to_release2/paragram-phrase-XXL.txt')

total = float(51409584)
ct = 0
fout = io.open(args.outfile, 'w', encoding='utf-8')
if args.filter != "scorer":
    with io.open('../to_release2/czeng-all.txt', 'r', encoding='utf-8') as f:
        for i in f:
            ln = i.strip()
            arr = i.split('\t')
            g = arr[0]
            tr = arr[1]
            key = arr[0] + "\t" + arr[1]
            gtoks = nltk.word_tokenize(g)
            gtr = nltk.word_tokenize(tr)
            if args.filter == "trans_score":
                o = float(arr[2])
                st = key +"\t{0}\n".format(o)
                #print st.encode("utf-8").strip()
                fout.write(st)
            elif args.filter == "uni":
                o = get_overlap(gtoks, gtr, "uni")
                st = key +"\t{0}\n".format(o)
                #print st.encode("utf-8").strip()
                fout.write(st)
            elif args.filter == "bi":
                o = get_overlap(gtoks, gtr, "bi")
                st = key +"\t{0}\n".format(o)
                #print st.encode("utf-8").strip()
                fout.write(st)
            elif args.filter == "tri":
                o = get_overlap(gtoks, gtr, "tri")
                st = key +"\t{0}\n".format(o)
                #print st.encode("utf-8").strip()
                fout.write(st)
            elif args.filter == "wordsim":
                gembs = [We[lookup(i),:] for i in gtoks]
                trembs = [We[lookup(i),:] for i in gtr]
                gemb = np.array(gembs).sum(axis=0) / len(gembs)
                tremb = np.array(trembs).sum(axis=0) /len(trembs)
                st = key +"\t{0}\n".format(-cosine(gemb, tremb) + 1)
                #print st.encode("utf-8").strip()
                fout.write(st)
            ct += 1
            if ct % 10000 == 0:
                print ct, ct/total

else:
    with io.open('../to_release2/czeng-all.txt', 'r', encoding='utf-8') as f:
        ct = 0
        batch = []
        keys = []
        for i in f:
            ln = i.strip()
            arr = i.split('\t')
            g = arr[0]
            tr = arr[1]
            key = arr[0] + "\t" + arr[1]
            gtoks = nltk.word_tokenize(g)
            gtr = nltk.word_tokenize(tr)
            tr1 = tree(" ".join(gtoks))
            tr2 = tree(" ".join(gtr))
            batch.append((tr1, tr2))
            keys.append(key)
            if len(batch) == 100:
                scores = score_batch(batch, ngram_words, wd_words)
                for n,i in enumerate(keys):
                    st = i + "\t{0}\n".format(scores[n])
                    fout.write(st)
            ct += 1
            if ct % 10000 == 0:
                print ct, ct / total
        if len(batch) > 0:
            scores = score_batch(batch, ngram_words, wd_words)
            for n, i in enumerate(keys):
                st = i + "\t{0}\n".format(scores[n])
                fout.write(st)
fout.close()