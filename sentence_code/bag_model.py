import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
from evaluate_bag import evaluate_all
import time
import utils
from lasagne_average_layer import lasagne_average_layer
import lasagne
import sys
import cPickle
import codecs

def checkIfQuarter(idx, n):
    # print idx, n
    if idx == round(n / 4.) or idx == round(n / 2.) or idx == round(3 * n / 4.):
        return True
    return False

class bag_model(object):

    # takes list of seqs, puts them in a matrix
    # returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        x_mask = np.asarray(x_mask, dtype=config.floatX)
        return x, x_mask

    def saveParams(self, fname):
        f = file(fname, 'wb')
        values = lasagne.layers.get_all_param_values(self.layer)
        #values = [i.get_value() for i in values]
        cPickle.dump(values, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def saveParams2(self, fname, words):
        f = codecs.open(fname, 'w', 'utf-8')
        values = lasagne.layers.get_all_param_values(self.layer)
        for i in words:
            wd = i.replace(" ","@@_")
            v = values[0][words[i],:]
            s = wd+" "
            for i in v:
                s += str(i)+" "
            s = s.strip()
            f.write(s+"\n")
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
            minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getpairs(self, batch, params):
        #batch is list of tuples
        g1 = []
        g2 = []

        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        embg1 = self.feedforward_function(g1x, g1mask)
        embg2 = self.feedforward_function(g2x, g2mask)

        #update representations
        for idx, i in enumerate(batch):
            i[0].representation = embg1[idx, :]
            i[1].representation = embg2[idx, :]

        pairs = utils.getPairsFast(batch, params.samplingtype)
        p1 = []
        p2 = []
        for i in pairs:
            p1.append(i[0].embeddings)
            p2.append(i[1].embeddings)

        p1x, p1mask = self.prepare_data(p1)
        p2x, p2mask = self.prepare_data(p2)

        return (g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask)

    def __init__(self, params, We=None):

        #symbolic params
        g1batchindices = T.imatrix()
        g2batchindices = T.imatrix()
        p1batchindices = T.imatrix()
        p2batchindices = T.imatrix()
        g1mask = T.matrix()
        g2mask = T.matrix()
        p1mask = T.matrix()
        p2mask = T.matrix()

        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=len(params.vocab), output_size=params.dim)
        if We is not None:
            l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=len(params.vocab), output_size=params.dim, W=We)

        l_out = lasagne_average_layer([l_emb, l_mask], tosum=False)

        self.final_layer = l_out
        embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=False)
        embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=False)
        embp1 = lasagne.layers.get_output(l_out, {l_in: p1batchindices, l_mask: p1mask}, deterministic=False)
        embp2 = lasagne.layers.get_output(l_out, {l_in: p2batchindices, l_mask: p2mask}, deterministic=False)

        t_embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=True)
        t_embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=True)

        def fix(x):
            return x*(x > 0) + 1E-10*(x <= 0)

        #objective function
        g1g2 = (embg1 * embg2).sum(axis=1)
        g1g2norm = T.sqrt(fix(T.sum(embg1 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg2 ** 2, axis=1)))
        g1g2 = g1g2 / g1g2norm

        p1g1 = (embp1 * embg1).sum(axis=1)
        p1g1norm = T.sqrt(fix(T.sum(embp1 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg1 ** 2, axis=1)))
        p1g1 = p1g1 / p1g1norm

        p2g2 = (embp2 * embg2).sum(axis=1)
        p2g2norm = T.sqrt(fix(T.sum(embp2 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg2 ** 2, axis=1)))
        p2g2 = p2g2 / p2g2norm

        costp1g1 = params.margin - g1g2 + p1g1
        costp1g1 = costp1g1 * (costp1g1 > 0)

        costp2g2 = params.margin - g1g2 + p2g2
        costp2g2 = costp2g2 * (costp2g2 > 0)

        cost = costp1g1 + costp2g2
        self.all_params = lasagne.layers.get_all_params(l_out, trainable=True)
        self.layer = l_out
        print self.all_params

        #regularization
        cost = T.mean(cost)

        g1g2 = (t_embg1 * t_embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(t_embg1 ** 2, axis=1)) * T.sqrt(T.sum(t_embg2 ** 2, axis=1))
        g1g2 = g1g2 / g1g2norm
        self.feedforward_function = theano.function([g1batchindices,g1mask], t_embg1)
        prediction = g1g2
        self.scoring_function = theano.function([g1batchindices, g2batchindices,
            g1mask, g2mask],prediction)

        #updates
        grads = theano.gradient.grad(cost, self.all_params)
        updates = params.learner(grads, self.all_params, params.eta)

        self.train_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                                                  g1mask, g2mask, p1mask, p2mask], cost, updates=updates)

        cost = costp1g1 + costp2g2
        cost = T.mean(cost)
        self.cost_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                                                  g1mask, g2mask, p1mask, p2mask], cost)

        print "Num Params:", utils.countParameters(self)

    #trains parameters
    def train(self, data, words, params, bpe, bpe_vocab):

        #pdb.set_trace()
        start_time = time.time()
        evaluate_all(self, words, params, bpe, bpe_vocab, params.bpe_overlap)

        counter = 0
        old_v = 0
        try:

            for eidx in xrange(params.epochs):

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(data), params.batchsize, shuffle=params.shuffle)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1

                    batch = [data[t] for t in train_index]

                    size = None
                    if params.wordtype == "2grams":
                        size = 2
                    elif params.wordtype == "3grams":
                        size = 3
                    elif params.wordtype == "4grams":
                        size = 4
                    elif params.wordtype == "5grams":
                        size = 5

                    for i in batch:
                        if params.wordtype == "words":
                            i[0].populate_embeddings(words, params.use_unktoken)
                            i[1].populate_embeddings(words, params.use_unktoken)
                        elif params.wordtype == "bpe":
                            i[0].populate_embeddings(words, params.use_unktoken)
                            i[1].populate_embeddings(words, params.use_unktoken)
                        elif "gram" in params.wordtype and params.overlap:
                            i[0].populate_embeddings_ngrams(words, size, params.use_unktoken)
                            i[1].populate_embeddings_ngrams(words, size, params.use_unktoken)
                        elif "gram" in params.wordtype and not params.overlap:
                            i[0].populate_embeddings_ngrams_nonverlap(words, size, params.use_unktoken)
                            i[1].populate_embeddings_ngrams_nonverlap(words, size, params.use_unktoken)

                    (g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask) = self.getpairs(batch, params)

                    t1 = time.time()
                    #for i in batch:
                    #    print i[0].phrase, len(i[0].embeddings)
                    #    print i[1].phrase, len(i[1].embeddings)

                    cost = self.train_function(g1x, g2x, p1x, p2x, g1mask, g2mask, p1mask, p2mask)
                    t2 = time.time()
                    print "cost, time: ", cost, t2-t1

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'
                        sys.exit(0)

                    if (checkIfQuarter(uidx, len(kf))):
                        if (params.evaluate):
                            v = evaluate_all(self, words, params, bpe, bpe_vocab, params.bpe_overlap)
                        if(params.save):
                            #counter += 1
                            #self.saveParams(params.outfile+str(counter)+'.pickle')
                            if v > old_v:
                                old_v = v
                                self.saveParams2(params.outfile + '.txt', words)

                    #undo batch to save RAM
                    for i in batch:
                        i[0].representation = None
                        i[1].representation = None
                        i[0].unpopulate_embeddings()
                        i[1].unpopulate_embeddings()

                        #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                if (params.evaluate):
                    v = evaluate_all(self, words, params, bpe, bpe_vocab, params.bpe_overlap)
                if (params.save):
                    # counter += 1
                    # self.saveParams(params.outfile+str(counter)+'.pickle')
                    if v > old_v:
                        old_v = v
                        self.saveParams2(params.outfile + '.txt', words)

                print 'Epoch ', (eidx + 1), 'Cost ', cost

        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)
