
from params import params
import utils
from bag_nmt_model import bag_nmt_model
import lasagne
import random
import numpy as np
import sys
import argparse
import pdb
from tree2 import tree
import cPickle
from theano import config
import nltk
import io


def getWordmap(textfile):
    words={}
    We = []
    f = io.open(textfile, 'r', encoding='utf-8')
    lines = f.readlines()
    if len(lines[0].split()) == 2:
        lines.pop(0)
    ct = 0
    for (n,i) in enumerate(lines):
        i=i.split()
        #if len(i) != 101:
        #    print "SKIPPED"
        #    continue
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=ct
        ct += 1
        #print i[0], n
        We.append(v)
    return (words, np.array(We))

words, We = getWordmap('../data/sl999_XXL_data.txt')

def getSeqs(p1,p2,words):
    p1 = p1.lower().split()
    lis = []
    for i in p1:
        idx = utils.lookupIDX(words, i)
        if idx == words["UUUNKKK"]:
            lis.append(i)
    p2 = p2.lower().split()
    for i in p2:
        idx = utils.lookupIDX(words, i)
        if idx == words["UUUNKKK"]:
            lis.append(i)
    return lis

def getCorrelation(words,f):
    f = io.open(f, 'r', encoding='utf-8')
    lines = f.readlines()
    lis = []
    for i in lines:
        i = i.split("\t")
        p1 = i[0]; p2 = i[1]; score = float(i[2])
        lis2 = getSeqs(p1, p2, words)
        lis.extend(lis2)
    return lis

def evaluate_all(words):
    prefix = "../datasets_tokenized/"

    farr = ["FNWN2013",#0
            "JHUppdb",#1
            "MSRpar2012",#2
            "MSRvid2012",#3
            "OnWN2012",#4
            "OnWN2013",#5
            "OnWN2014",#6
            "SMT2013",#7
            "SMTeuro2012",#8
            "SMTnews2012",#9
            "anno-dev",#10
            "anno-test",#11
            "answer-forum2015",#12
            "answer-student2015",#13
            "belief2015",#14
            "bigram-jn",#15
            "bigram-nn",#16
            "bigram-vn",#17
            "deft-forum2014",#18
            "deft-news2014",#19
            "headline2013",#20
            "headline2014",#21
            "headline2015",#22
            "images2014",#23
            "images2015",#24
            "sicktest",#25
            "tweet-news2014",#26
            "twitter",#27
            "headlines2016",#28
            "plagarism2016",#29
            "postediting2016",#30
            "question2016",#31
            "answer2016",#32
            "sts-dev.proc",#33
            "sts-test.proc"]#34

    unk_lis = []
    for i in farr:
        lis = getCorrelation(words,prefix+i)
        print i, len(lis)
        unk_lis.extend(lis)
    #pdb.set_trace()
    print unk_lis
    print len(unk_lis)

evaluate_all(words)