from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import utils
import apply_bpe
from tree2 import tree
import io
import numpy as np
import pdb

cache = {}

def getSeqs(p1,p2,words,params):
    size = None

    if p1 in cache and p2 in cache:
        return cache[p1].embeddings, cache[p2].embeddings

    _p1 = p1
    _p2 = p2

    p1 = tree(p1)
    p2 = tree(p2)

    if params.wordtype == "words":
        p1.populate_embeddings(words, True)
        p2.populate_embeddings(words, True)
    else:
        p1.populate_embeddings_ngrams(words, 3, True)
        p2.populate_embeddings_ngrams(words, 3, True)

    cache[_p1]=p1
    cache[_p2]=p2

    return p1.embeddings, p2.embeddings

def getCorrelation(model,words,f,params):
    f = io.open(f, 'r', encoding='utf-8')
    lines = f.readlines()
    preds = []
    golds = []
    seq1 = []
    seq2 = []
    ct = 0
    for i in lines:
        i = i.split("\t")
        p1 = i[0]; p2 = i[1]; score = float(i[2])
        X1, X2 = getSeqs(p1,p2,words,params)
        #pdb.set_trace()
        seq1.append(X1)
        seq2.append(X2)
        ct += 1
        if ct % 100 == 0:
            #pdb.set_trace()
            x1,m1 = model.prepare_data(seq1)
            x2,m2 = model.prepare_data(seq2)
            scores = model.scoring_function(x1,x2,m1,m2)
            scores = np.squeeze(scores)
            preds.extend(scores.tolist())
            seq1 = []
            seq2 = []
        golds.append(score)
    if len(seq1) > 0:
        x1,m1 = model.prepare_data(seq1)
        x2,m2 = model.prepare_data(seq2)
        scores = model.scoring_function(x1,x2,m1,m2)
        scores = np.squeeze(scores)
        preds.extend(scores.tolist())
    return pearsonr(preds,golds)[0], spearmanr(preds,golds)[0]

def evaluate_all(model,words,params):
    prefix = "../datasets_tokenized2/"
    parr = []; sarr = []; farr = []

    farr = ["FNWN2013",  #0
            "JHUppdb",  #1
            "MSRpar2012",  #2
            "MSRvid2012",  #3
            "OnWN2012",  #4
            "OnWN2013",  #5
            "OnWN2014",  #6
            "SMT2013",  #7
            "SMTeuro2012",  #8
            "SMTnews2012",  #9
            "anno-dev",  #10
            "anno-test",  #11
            "answer-forum2015",  #12
            "answer-student2015",  #13
            "belief2015",  #14
            "bigram-jn",  #15
            "bigram-nn",  #16
            "bigram-vn",  #17
            "deft-forum2014",  #18
            "deft-news2014",  #19
            "headline2013",  #20
            "headline2014",  #21
            "headline2015",  #22
            "images2014",  #23
            "images2015",  #24
            "sicktest",  #25
            "tweet-news2014",  #26
            "twitter",  #27
            "headlines2016",  #28
            "plagarism2016",  #29
            "postediting2016",  #30
            "question2016",  #31
            "answer2016",  #32
            "STS2017",  # 33
            "sts-dev.proc",  #34
            "sts-test.proc"]#35

    for i in farr:
        p,s = getCorrelation(model,words,prefix+i,params)
        parr.append(p); sarr.append(s)

    s = ""
    for i,j,k in zip(parr,sarr,farr):
        s += str(i)+" "+str(j)+" "+k+" | "

    n = parr[2]+ parr[3]+parr[4]+parr[8]+parr[9]
    n = n/5.
    s += str(n)+" 2012-average | "

    n = parr[0]+ parr[5]+parr[7]+parr[20]
    n = n/4.
    s += str(n)+" 2013-average | "

    n = parr[6]+ parr[18]+parr[19]+parr[21]+parr[23]+parr[26]
    n = n/6.
    s += str(n)+" 2014-average | "

    n = parr[12]+ parr[13]+parr[14]+parr[22]+parr[24]
    n = n/5.
    s += str(n)+" 2015-average | "

    n = parr[28]+ parr[29]+parr[30]+parr[31]+parr[32]
    n = n/5.
    v = n
    s += str(n)+" 2016-average | "

    n1 = parr[12]+ parr[13]+parr[14]+parr[22]+parr[24]
    n2 = parr[6]+ parr[18]+parr[19]+parr[21]+parr[23]+parr[26]
    n3 = parr[0]+ parr[5]+parr[7]+parr[20]
    n4 = parr[2]+ parr[3]+parr[4]+parr[8]+parr[9]
    n5 = parr[25] + parr[27]
    n = (n1 + n2 + n3 + n4 + n5) / 22.
    s += "\n" + str(n)+" total | "
    s += "\n" + str(parr[33]) + " " + str(parr[34])
    print s
    return v


