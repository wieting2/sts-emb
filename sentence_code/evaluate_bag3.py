from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import utils
import apply_bpe
from tree2 import tree
import io
import numpy as np
import pdb

ngram_cache = {}
word_cache = {}
lstm_cache = {}

def getSeqs(p1,p2,ngram_words,wd_words,params):

    if params.combo_type == "ngram-word":
        if p1 in ngram_cache and p2 in ngram_cache and p1 in word_cache and p2 in word_cache:
            return ngram_cache[p1].embeddings, word_cache[p1].embeddings, \
                   ngram_cache[p2].embeddings, word_cache[p2].embeddings

        _p1 = p1
        _p2 = p2

        np1 = tree(p1)
        np2 = tree(p2)

        wp1 = tree(p1)
        wp2 = tree(p2)

        np1.populate_embeddings_ngrams(ngram_words, 3, True)
        np2.populate_embeddings_ngrams(ngram_words, 3, True)
        wp1.populate_embeddings(wd_words, True)
        wp2.populate_embeddings(wd_words, True)

        ngram_cache[_p1]=np1
        ngram_cache[_p2]=np2

        word_cache[_p1]=wp1
        word_cache[_p2]=wp2

        return np1.embeddings, wp1.embeddings, np2.embeddings, wp2.embeddings

    elif params.combo_type == "ngram-lstm":
        if p1 in ngram_cache and p2 in ngram_cache and p1 in lstm_cache and p2 in lstm_cache:
            return ngram_cache[p1].embeddings, lstm_cache[p1].embeddings, \
                   ngram_cache[p2].embeddings, lstm_cache[p2].embeddings

        _p1 = p1
        _p2 = p2

        np1 = tree(p1)
        np2 = tree(p2)

        wp1 = tree(p1)
        wp2 = tree(p2)

        np1.populate_embeddings_ngrams(ngram_words, 3, True)
        np2.populate_embeddings_ngrams(ngram_words, 3, True)
        wp1.populate_embeddings(wd_words, True)
        wp2.populate_embeddings(wd_words, True)

        ngram_cache[_p1] = np1
        ngram_cache[_p2] = np2

        lstm_cache[_p1] = wp1
        lstm_cache[_p2] = wp2

        return np1.embeddings, wp1.embeddings, np2.embeddings, wp2.embeddings
    elif params.combo_type == "word-lstm":
        if p1 in word_cache and p2 in word_cache and p1 in lstm_cache and p2 in lstm_cache:
            return word_cache[p1].embeddings, lstm_cache[p1].embeddings, \
                   word_cache[p2].embeddings, lstm_cache[p2].embeddings

        _p1 = p1
        _p2 = p2

        np1 = tree(p1)
        np2 = tree(p2)

        wp1 = tree(p1)
        wp2 = tree(p2)

        np1.populate_embeddings(wd_words, True)
        np2.populate_embeddings(wd_words, True)
        wp1.populate_embeddings(wd_words, True)
        wp2.populate_embeddings(wd_words, True)

        word_cache[_p1] = np1
        word_cache[_p2] = np2

        lstm_cache[_p1] = wp1
        lstm_cache[_p2] = wp2

        return np1.embeddings, wp1.embeddings, np2.embeddings, wp2.embeddings
    elif params.combo_type == "ngram-word-lstm":
        if p1 in word_cache and p2 in word_cache and p1 in lstm_cache and p2 in lstm_cache \
                and p1 in ngram_cache and p2 in ngram_cache:
            return ngram_cache[p1].embeddings, word_cache[p1].embeddings, \
                   lstm_cache[p1].embeddings, ngram_cache[p2].embeddings, \
                   word_cache[p2].embeddings, lstm_cache[p2].embeddings

        _p1 = p1
        _p2 = p2

        np1 = tree(p1)
        np2 = tree(p2)

        wp1 = tree(p1)
        wp2 = tree(p2)

        lp1 = tree(p1)
        lp2 = tree(p2)

        np1.populate_embeddings_ngrams(ngram_words, 3, True)
        np2.populate_embeddings_ngrams(ngram_words, 3, True)
        wp1.populate_embeddings(wd_words, True)
        wp2.populate_embeddings(wd_words, True)
        lp1.populate_embeddings(wd_words, True)
        lp2.populate_embeddings(wd_words, True)

        ngram_cache[_p1] = np1
        ngram_cache[_p2] = np2

        word_cache[_p1] = wp1
        word_cache[_p2] = wp2

        lstm_cache[_p1] = lp1
        lstm_cache[_p2] = lp2

        return np1.embeddings, wp1.embeddings, lp1.embeddings, np2.embeddings, wp2.embeddings, lp2.embeddings

def getCorrelation(model,ngram_words,wd_words,f,params):
    f = io.open(f, 'r', encoding='utf-8')
    lines = f.readlines()
    preds = []
    golds = []
    seq1n = []
    seq1w = []
    seq1l = []
    seq2n = []
    seq2w = []
    seq2l = []
    ct = 0
    for i in lines:
        i = i.split("\t")
        p1 = i[0]; p2 = i[1]; score = float(i[2])
        if params.combo_type != "ngram-word-lstm":
            nX1, wX1, nX2, wX2 = getSeqs(p1,p2,ngram_words,wd_words,params)
        else:
            nX1, wX1, lX1, nX2, wX2, lX2 = getSeqs(p1, p2, ngram_words, wd_words, params)
        #pdb.set_trace()
        seq1n.append(nX1)
        seq1w.append(wX1)
        seq2n.append(nX2)
        seq2w.append(wX2)
        if params.combo_type == "ngram-word-lstm":
            seq1l.append(lX1)
            seq2l.append(lX2)
        ct += 1
        if ct % 100 == 0:
            #pdb.set_trace()
            nx1, nm1 = model.prepare_data(seq1n)
            wx1, wm1 = model.prepare_data(seq1w)
            nx2, nm2 = model.prepare_data(seq2n)
            wx2, wm2 = model.prepare_data(seq2w)
            if params.combo_type == "ngram-word-lstm":
                lx1, lm1 = model.prepare_data(seq1l)
                lx2, lm2 = model.prepare_data(seq2l)
            if params.combo_type != "ngram-word-lstm":
                scores = model.scoring_function(nx1,nm1,wx1,wm1,nx2,nm2,wx2,wm2)
            else:
                scores = model.scoring_function(nx1, nm1, wx1, wm1, lx1, lm1,
                                                nx2, nm2, wx2, wm2, lx2, lm2)
            scores = np.squeeze(scores)
            preds.extend(scores.tolist())
            seq1n = []
            seq1w = []
            seq2n = []
            seq2w = []
            seq1l = []
            seq2l = []
        golds.append(score)
    if len(seq1n) > 0:
        nx1, nm1 = model.prepare_data(seq1n)
        wx1, wm1 = model.prepare_data(seq1w)
        nx2, nm2 = model.prepare_data(seq2n)
        wx2, wm2 = model.prepare_data(seq2w)
        if params.combo_type == "ngram-word-lstm":
            lx1, lm1 = model.prepare_data(seq1l)
            lx2, lm2 = model.prepare_data(seq2l)
        if params.combo_type != "ngram-word-lstm":
            scores = model.scoring_function(nx1, nm1, wx1, wm1, nx2, nm2, wx2, wm2)
        else:
            scores = model.scoring_function(nx1, nm1, wx1, wm1, lx1, lm1,
                                            nx2, nm2, wx2, wm2, lx2, lm2)
        scores = np.squeeze(scores)
        preds.extend(scores.tolist())
    return pearsonr(preds,golds)[0], spearmanr(preds,golds)[0]

def evaluate_all(model,ngram_words,wd_words,params):
    prefix = "../datasets_tokenized2/"
    parr = []; sarr = []; farr = []

    farr = ["FNWN2013",  #0
            "JHUppdb",  #1
            "MSRpar2012",  #2
            "MSRvid2012",  #3
            "OnWN2012",  #4
            "OnWN2013",  #5
            "OnWN2014",  #6
            "SMT2013",  #7
            "SMTeuro2012",  #8
            "SMTnews2012",  #9
            "anno-dev",  #10
            "anno-test",  #11
            "answer-forum2015",  #12
            "answer-student2015",  #13
            "belief2015",  #14
            "bigram-jn",  #15
            "bigram-nn",  #16
            "bigram-vn",  #17
            "deft-forum2014",  #18
            "deft-news2014",  #19
            "headline2013",  #20
            "headline2014",  #21
            "headline2015",  #22
            "images2014",  #23
            "images2015",  #24
            "sicktest",  #25
            "tweet-news2014",  #26
            "twitter",  #27
            "headlines2016",  #28
            "plagarism2016",  #29
            "postediting2016",  #30
            "question2016",  #31
            "answer2016",  #32
            "STS2017",  # 33
            "sts-dev.proc",  #34
            "sts-test.proc"]#35


    for i in farr:
        p,s = getCorrelation(model,ngram_words,wd_words,prefix+i,params)
        parr.append(p); sarr.append(s)

    s = ""
    for i,j,k in zip(parr,sarr,farr):
        s += str(i)+" "+str(j)+" "+k+" | "

    n = parr[2]+ parr[3]+parr[4]+parr[8]+parr[9]
    n = n/5.
    s += str(n)+" 2012-average | "

    n = parr[0]+ parr[5]+parr[7]+parr[20]
    n = n/4.
    s += str(n)+" 2013-average | "

    n = parr[6]+ parr[18]+parr[19]+parr[21]+parr[23]+parr[26]
    n = n/6.
    s += str(n)+" 2014-average | "

    n = parr[12]+ parr[13]+parr[14]+parr[22]+parr[24]
    n = n/5.
    s += str(n)+" 2015-average | "

    n = parr[28]+ parr[29]+parr[30]+parr[31]+parr[32]
    n = n/5.
    v = n
    s += str(n)+" 2016-average | "

    n1 = parr[12]+ parr[13]+parr[14]+parr[22]+parr[24]
    n2 = parr[6]+ parr[18]+parr[19]+parr[21]+parr[23]+parr[26]
    n3 = parr[0]+ parr[5]+parr[7]+parr[20]
    n4 = parr[2]+ parr[3]+parr[4]+parr[8]+parr[9]
    n5 = parr[25] + parr[27]
    n = (n1 + n2 + n3 + n4 + n5) / 22.
    s += "\n" + str(n)+" total | "
    s += "\n" + str(parr[34]) + " " + str(parr[35])
    print s
    return v