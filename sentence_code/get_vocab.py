from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import utils
import apply_bpe
from tree2 import tree
import io
import numpy as np
import pdb

def getWords(f):
    f = io.open(f, 'r', encoding='utf-8')
    lines = f.readlines()
    words = set([])

    for i in lines:
        i = i.lower().split("\t")
        p1 = i[0]; p2 = i[1]
        wds = p1.split() + p2.split()
        for j in wds:
            words.add(j)
    return list(words)

def evaluate_all():
    prefix = "../datasets_tokenized2/"
    words = []

    farr = ["FNWN2013",  #0
            "JHUppdb",  #1
            "MSRpar2012",  #2
            "MSRvid2012",  #3
            "OnWN2012",  #4
            "OnWN2013",  #5
            "OnWN2014",  #6
            "SMT2013",  #7
            "SMTeuro2012",  #8
            "SMTnews2012",  #9
            "anno-dev",  #10
            "anno-test",  #11
            "answer-forum2015",  #12
            "answer-student2015",  #13
            "belief2015",  #14
            "bigram-jn",  #15
            "bigram-nn",  #16
            "bigram-vn",  #17
            "deft-forum2014",  #18
            "deft-news2014",  #19
            "headline2013",  #20
            "headline2014",  #21
            "headline2015",  #22
            "images2014",  #23
            "images2015",  #24
            "sicktest",  #25
            "tweet-news2014",  #26
            "twitter",  #27
            "headlines2016",  #28
            "plagarism2016",  #29
            "postediting2016",  #30
            "question2016",  #31
            "answer2016",  #32
            "STS2017",  # 33
            "sts-dev.proc",  #34
            "sts-test.proc"]#35

    for i in farr:
        words.extend(getWords(prefix + i))

    words = set(words)

    words = list(words)
    words.sort()
    for j in words:
        print j.encode("utf-8")

    #print len(words)

evaluate_all()
