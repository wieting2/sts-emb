
import lasagne
from theano import tensor as T

class lasagne_gate_layer(lasagne.layers.MergeLayer):

    def __init__(self, incoming, dim, type, **kwargs):
        super(lasagne_gate_layer, self).__init__(incoming, **kwargs)
        self.dim = dim
        self.type = type

    def get_output_for(self, inputs, **kwargs):
        emb = inputs[0]
        gate = inputs[1]
        if self.type == 1:
            input = gate*emb
            p1 = input[:,0:self.dim]
            p2 = input[:,self.dim::]
            return p1 + p2
        elif self.type == 2:
            w1 = gate[:,0].dimshuffle(0,'x')
            w2 = gate[:,1].dimshuffle(0,'x')
            p1 = emb[:,0:self.dim]
            p2 = emb[:,self.dim::]
            return w1*p1 + w2*p2

    def get_output_shape_for(self, input_shape):
        d1,d2 = input_shape[0]
        return d1, d2/2