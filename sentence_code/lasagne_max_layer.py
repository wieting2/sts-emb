
import lasagne
from theano import tensor as T

class lasagne_max_layer(lasagne.layers.MergeLayer):

    def __init__(self, incoming, params, **kwargs):
        super(lasagne_max_layer, self).__init__(incoming, **kwargs)
        self.dim = params.dim

    def get_output_for(self, inputs, **kwargs):
        emb = inputs[0]
        mask = inputs[1]
        emb = (emb * mask[:, :, None])
        emb += ((mask - 1) * 100)[:, :, None]
        return T.max(emb, axis=1)