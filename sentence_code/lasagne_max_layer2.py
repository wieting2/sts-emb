
import lasagne
from theano import tensor as T

class lasagne_max_layer2(lasagne.layers.MergeLayer):

    def __init__(self, incoming, **kwargs):
        super(lasagne_max_layer2, self).__init__(incoming, **kwargs)

    def get_output_for(self, inputs, **kwargs):
        emb = inputs[0]
        return T.max(emb, axis=1)