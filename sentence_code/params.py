class params(object):
    def __init__(self):
        self.LW = 1
        self.LC = 1
        self.eta = 0.001

    def __str__(self):
        t = "LW", self.LW, ", LC", self.LC, ", eta", self.eta
        t = map(str, t)
        return ' '.join(t)