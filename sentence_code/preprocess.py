import random
import numpy as np
import argparse
import nltk
import io
import time

def getData(params):
    lines = io.open(params.dataf, 'r', encoding='utf-8').readlines()
    lines2 = []
    examples = []
    for i in lines:
        s1 = i.split("\t")[0].lower()
        s2 = i.split("\t")[1].lower()
        if s1 == s2:
            continue
        s1 = nltk.word_tokenize(s1)
        s2 = nltk.word_tokenize(s2)
        if len(s1) < 30 and len(s2) < 30:
            lines2.append((" ".join(s1), " ".join(s2)))
    random.shuffle(lines2)
    return lines2

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

data = []

random.seed(1)
np.random.seed(1)

parser = argparse.ArgumentParser()
parser.add_argument("-dataf")
args = parser.parse_args()

params = args

t1 = time.time()
data = getData(params)
print (time.time() - t1)

fout = open('long_data_preprocessed.txt', 'w')
for i in data:
    fout.write(i[0] + "\t" + i[1] + "\n")
fout.close()