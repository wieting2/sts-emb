
import numpy as np
import lasagne
from theano import tensor as T
import theano
import pdb
import io
from theano import config

'''
tensor = T.tensor3()
inp  = lasagne.layers.InputLayer((None, None, None))
pool = lasagne.layers.MaxPool1DLayer(inp, 3, stride=1, pad=0, ignore_border=True)
outp = lasagne.layers.get_output(pool, {inp: tensor}, deterministic=False)

f = theano.function([tensor], outp)

x = np.random.random((2,4,3))

print x
print f(x).squeeze()
print f(x).squeeze().shape
'''

'''
def max_pool(tensor):
    return np.max(tensor,axis=1)

def max_pool2(mask, tensor):
    temp = (tensor * mask[:, :, None])
    temp += ((mask - 1) * 10)[:, :, None]
    return np.max(tensor,axis=1)

x = np.random.random((2,4,3))*2 - 1
mask = np.array(([[1,1,1,0],[1,0,0,0]]))
print x
print max_pool2(mask, x)
'''

def getWordmap(textfile):
    words={}
    We = []
    f = io.open(textfile, 'r', encoding='utf-8')
    lines = f.readlines()
    if len(lines[0].split()) == 2:
        lines.pop(0)
    ct = 0
    for (n,i) in enumerate(lines):
        i=i.split()
        #if len(i) != 101:
        #    print "SKIPPED"
        #    continue
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=ct
        ct += 1
        #print i[0], n
        We.append(v)
    return (words, np.array(We))

words, We = getWordmap('../data/sl999_XXL_data.txt')

We = theano.shared(np.asarray(We, dtype=config.floatX))

g = T.tensor3()
mask = T.matrix()

l_in = lasagne.layers.InputLayer((None, None, 3))
l_mask = lasagne.layers.InputLayer(shape=(None, None))
#l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1],
#                                      W=We)
l_lstm = lasagne.layers.LSTMLayer(l_in, 3, peepholes=True, learn_init=False,
                                              mask_input=l_mask)
l_lstmb = lasagne.layers.LSTMLayer(l_in, 3, learn_init=False,
                                                  mask_input=l_mask, backwards=True)
emb1 = lasagne.layers.get_output(l_lstm, {l_in: g, l_mask: mask}, deterministic=False)
emb2 = lasagne.layers.get_output(l_lstmb, {l_in: g, l_mask: mask}, deterministic=False)

emb1 = (emb1 * mask[:, :, None])
emb1 += ((mask - 1) * 100)[:, :, None]

emb2 = (emb2 * mask[:, :, None])
emb2 += ((mask - 1) * 100)[:, :, None]

emb = T.concatenate([emb1, emb2], axis=1)

emb2 = T.max(emb, axis=1)

f = theano.function([g, mask], [emb, emb2])
#f = theano.function([g, mask], [emb1, emb2])

x = np.random.random((2,4,3))*2 - 1
mask = np.array(([[1,1,1,0],[1,0,0,0]]))

#e1, e2 = f(x, mask)
e1, e2 = f(x, mask)

pdb.set_trace()