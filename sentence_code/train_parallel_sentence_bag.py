
from params import params
import utils
from bag_model import bag_model
import lasagne
import random
import numpy as np
import sys
import argparse
import pdb
from tree2 import tree
import cPickle
from theano import config

f1 = '../data/simple.aligned'
f2 = '../data/normal.aligned'

def get_lines(f):
    f = open(f,'r')
    lines = f.readlines()
    d = []
    for i in lines:
        i = i.split('\t')
        d.append(i[-1].strip().lower())
    return d

def getData(lines):
    examples = []
    for i in lines:
        e = (tree(i[0]), tree(i[1]))
        examples.append(e)
    return examples

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

def str2act(v):
    if v is None:
        return v
    if v.lower() == "tanh":
        return lasagne.nonlinearities.tanh
    if v.lower() == "linear":
        return lasagne.nonlinearities.linear
    if v.lower() == "relu":
        return lasagne.nonlinearities.rectify
    raise ValueError('A type that was supposed to be a learner is not.')

def get_ngrams(examples, size, cutoff):
    d = {}
    for i in examples:
        e1 = " " + i[0].phrase + " "
        e2 = " " + i[1].phrase +" "
        for j in range(len(e1)):
            ngram = e1[j:j+size]
            if len(ngram) != size:
                continue
            if ngram in d:
                d[ngram] += 1
            else:
                d[ngram] = 1
        for j in range(len(e2)):
            ngram = e2[j:j+size]
            if len(ngram) != size:
                continue
            if ngram in d:
                d[ngram] += 1
            else:
                d[ngram] = 1
    feature_map = {}
    idx = 0
    for i in d:
        if d[i] > cutoff:
            feature_map[i] = idx
            idx += 1
    feature_map["UUUNKKK"] = idx
    return feature_map

def get_words(examples, cutoff):
    d = {}
    for i in examples:
        e1 = i[0].phrase.split()
        e2 = i[1].phrase.split()
        for j in range(len(e1)):
            wd = e1[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
        for j in range(len(e2)):
            wd = e2[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
    feature_map = {}
    idx = 0
    for i in d:
        if d[i] > cutoff:
            feature_map[i] = idx
            idx += 1
    feature_map["UUUNKKK"] = idx
    return feature_map

data = []

d1 = get_lines(f1)
d2 = get_lines(f2)

for i in range(len(d1)):
    data.append((d1[i],d2[i]))

random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LC", help="Regularization on Composition Parameters", type=float)
parser.add_argument("-LW", help="Regularization on Embedding Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.")
parser.add_argument("-dataf", help="Training data file.")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.")
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-fraction", help="Percent of training examples to use.", type=float)
parser.add_argument("-clip", help="float to indicate grad cutting. Use 0 for no clipping.",type=int)
parser.add_argument("-eta", help="learning rate",type=float)
parser.add_argument("-learner", help="Either AdaGrad or Adam")
parser.add_argument("-featurefile", help="File containing n-grams and their counts.")
parser.add_argument("-featurefileWord", help="File containing n-grams and their counts.")
parser.add_argument("-featurefilePhrase", help="File containing n-grams and their counts.")
parser.add_argument("-cutoff", help="Above or equal to this, features are kept.", type=int, default=0)
parser.add_argument("-offset", help="n in n-gram for features.", type=int)
parser.add_argument("-layers", help="Number of layers <= 2.", type=int)
parser.add_argument("-act1", help="activation of last layer of DSSM.")
parser.add_argument("-win", help="Number of words to include in context in the left and the right.", type=int)
parser.add_argument("-character_file", help="List of characters for embeddings.")
parser.add_argument("-outgate", help="Outgate")
parser.add_argument("-dropout", help="dropout", type=float)
parser.add_argument("-conv_type", help="conv_type", type=int)
parser.add_argument("-actconv")
parser.add_argument('-noshuffle', default="False")
parser.add_argument('-sum')
parser.add_argument("-model", help="", type=int)
parser.add_argument("-ngramtype", help="", type=int)
parser.add_argument("-charmode", help="")
parser.add_argument("-ls", help="", type=int)
parser.add_argument("-mode", help="")
parser.add_argument("-average", help="")
parser.add_argument("-lam", help="", type=float)
parser.add_argument("-alpha", help="", type=float)
parser.add_argument("-beta", help="", type=float)
parser.add_argument("-dim", help="", type=int)
parser.add_argument("-scramble",type=float, default=0.)
parser.add_argument("-dropout1",type=float, default=0.)
parser.add_argument("-dropout2",type=float, default=0.)
parser.add_argument("-prebatch")
parser.add_argument("-scramble_single")
parser.add_argument("-partial")
parser.add_argument("-model_type",type=int)
parser.add_argument("-sumlayer")
parser.add_argument("-shuffle_exp")
parser.add_argument("-mixed_type")
parser.add_argument("-short")
parser.add_argument("-max",type=int,default=0)
parser.add_argument("-shuffle", default="true")
parser.add_argument("-use_unktoken", default="false")
parser.add_argument("-wordtype", default="word", help = "words, 2grams, 3grams, 4grams, be, beoverlap, wp, wpoverlap")
parser.add_argument("-overlap", default = "false")
parser.add_argument("-load_embeddings", default = None)

args = parser.parse_args()

params.LC = args.LC
params.LW = args.LW
params.outfile = args.outfile
params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.wordfile = args.wordfile
params.nntype = args.nntype
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.dataf = args.dataf
params.margin = args.margin
params.type = args.samplingtype
params.epochs = args.epochs
params.evaluate = str2bool(args.evaluate)
params.learner = str2learner(args.learner)
params.eta = args.eta
params.featurefile = args.featurefile
params.featurefileWord = args.featurefileWord
params.featurefilePhrase = args.featurefilePhrase
params.cutoff = args.cutoff
params.offset = args.offset
params.numlayers = args.layers
params.act = str2act(args.act1)
params.nntype = args.nntype
params.win = args.win
params.outgate = str2bool(args.outgate)
params.character_file = args.character_file
params.conv_type = args.conv_type
params.dropout = args.dropout
params.clip = args.clip
params.outfile = '../models/'+args.outfile
params.actconv = str2act(args.actconv)
params.noshuffle = str2bool(args.noshuffle)
params.sum = str2bool(args.sum)
params.model = args.model
params.nntype = args.nntype
params.ngramtype = args.ngramtype
params.charmode = args.charmode
params.ls = args.ls
params.mode = args.mode
params.average = str2bool(args.average)
params.lam = args.lam
params.alpha = args.alpha
params.beta = args.beta
params.dim = args.dim
params.scramble = args.scramble
params.dropout1 = args.dropout1
params.dropout2 = args.dropout2
params.prebatch = str2bool(args.prebatch)
params.scramble_single = str2bool(args.scramble_single)
params.partial = str2bool(args.partial)
params.model_type = args.model_type
params.sumlayer = str2bool(args.sumlayer)
params.shuffle_exp = str2bool(args.shuffle_exp)
params.mixed_type = str2bool(args.mixed_type)
params.short = str2bool(args.short)
params.max = args.max
params.shuffle = str2bool(args.shuffle)
params.use_unktoken = str2bool(args.use_unktoken)
params.wordtype = args.wordtype
params.overlap = str2bool(args.overlap)
params.load_embeddings = args.load_embeddings

data = getData(data)

if params.mode == "ppdb":
    d = utils.getData("../data/ppdb-XL-ordered-data.txt",None)
    random.shuffle(d)
    ct = 0
    for i in data:
        ct += len(i[0].phrase.split()) + len(i[1].phrase.split())
    data = []
    print ct
    idx = 0
    ct2 = 0
    while ct > 0:
        dd = d[idx]
        data.append(dd)
        v = len(dd[0].phrase.split()) + len(dd[1].phrase.split())
        ct -= v
        ct2 += v
        idx += 1
    print ct2

words = None

if params.wordtype == "words":
    words = get_words(data, params.cutoff)
elif params.wordtype == "2grams":
    words = get_ngrams(data, 2, params.cutoff)
elif params.wordtype == "3grams":
    words = get_ngrams(data, 3, params.cutoff)
elif params.wordtype == "4grams":
    words = get_ngrams(data, 4, params.cutoff)

# if args.load_embeddings:
#     f = open('../embedding_expts/word2idx.txt','r')
#     lines = f.readlines()
#     d = {}
#     for i in lines:
#         i = i.split('\t')
#         d[i[0]] = int(i[1])
#     p = cPickle.load(open(args.load_embeddings,'rb'))
#     p = p[0]
#     m = lasagne.init.Uniform(1).sample((len(words),params.dim))
#     arr = []
#     idx = len(d)
#     for i in words:
#         if i not in d:
#             d[i] = idx
#             idx += 1
#             arr.append(m[words[i],:])
#     arr = np.array(arr)
#     We = np.concatenate([p,arr])
#     words = d

if args.load_embeddings:
    m = np.load('../data/twitter_unigrams.emb.npy')
    v = np.load('../data/twitter_unigrams.voc.npy')
    d = {}
    for i in range(len(v)):
        d[v[i]]=i
    d["UUUNKKK"] = len(v)
    m2 = lasagne.init.Uniform(1).sample((1, params.dim))
    We = np.concatenate([m,m2])
    We = np.asarray(We, dtype=config.floatX)
    words = d

params.vocab = words
model = bag_model(params)

if args.load_embeddings:
    lasagne.layers.set_all_param_values(model.layer, [We])

print " ".join(sys.argv)
print "Num examples:", len(data)
print "Num words:", len(words)

model.train(data, words, params)

