
from params import params
import utils
from bag_model2 import bag_model
import lasagne
import random
import numpy as np
import sys
import argparse
import pdb
from tree2 import tree
import cPickle
from theano import config

f1 = '../data/simple.aligned'
f2 = '../data/normal.aligned'

def get_lines(f):
    f = open(f,'r')
    lines = f.readlines()
    d = []
    for i in lines:
        i = i.split('\t')
        d.append(i[-1].strip().lower())
    return d

def getData(lines):
    examples = []
    for i in lines:
        e = (tree(i[0]), tree(i[1]))
        examples.append(e)
    return examples

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

def str2act(v):
    if v is None:
        return v
    if v.lower() == "tanh":
        return lasagne.nonlinearities.tanh
    if v.lower() == "linear":
        return lasagne.nonlinearities.linear
    if v.lower() == "relu":
        return lasagne.nonlinearities.rectify
    raise ValueError('A type that was supposed to be a learner is not.')

data = []

d1 = get_lines(f1)
d2 = get_lines(f2)

for i in range(len(d1)):
    data.append((d1[i],d2[i]))

random.seed(1)
np.random.seed(1)

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Embedding Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-dim", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-vocabfile", help="Word file to be read in.")
parser.add_argument("-save", help="Whether to pickle the model.", default="false")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.", default="MAX")
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.", default="True")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-eta", help="learning rate", type=float, default=0.001)
parser.add_argument("-learner", help="Either AdaGrad or Adam", default="Adam")
parser.add_argument("-outgate", help="Outgate")
parser.add_argument("-dropout", help="dropout", type=float)
parser.add_argument("-model", help="", type=int)
parser.add_argument("-mode", help="", default="sentence")
parser.add_argument("-model_type", type=int)
parser.add_argument("-max", type=int, default=0)
parser.add_argument("-load_pickle")

args = parser.parse_args()

args.save = str2bool(args.save)
args.evaluate = str2bool(args.evaluate)
args.learner = str2learner(args.learner)
args.outgate = str2bool(args.outgate)

params = args

data = getData(data)

if params.mode == "ppdb":
    d = utils.getData("../data/ppdb-XL-ordered-data.txt",None)
    random.shuffle(d)
    ct = 0
    for i in data:
        ct += len(i[0].phrase.split()) + len(i[1].phrase.split())
    data = []
    print ct
    idx = 0
    ct2 = 0
    while ct > 0:
        dd = d[idx]
        data.append(dd)
        v = len(dd[0].phrase.split()) + len(dd[1].phrase.split())
        ct -= v
        ct2 += v
        idx += 1
    print ct2

def getWordmap(wordfile, vocabfile):
    words={}
    We = []
    vocab = set([])
    f = open(vocabfile, 'r')
    lines = f.readlines()
    for i in lines:
        vocab.add(i.split()[0].strip())
    idx = 0
    with open(wordfile) as f:
        for i in f:
            i=i.split()
            if len(i) == 2:
                continue
            j = 1
            v = []
            while j < len(i):
                v.append(float(i[j]))
                j += 1
            if i[0] in vocab:
                words[i[0]]=idx
                We.append(v)
                idx += 1
    words["UUUNKKK"] = len(words)
    We = np.asarray(We, dtype=config.floatX)
    #m2 = lasagne.init.Uniform(1).sample((1, params.dim))
    #We = np.concatenate([We, m2])
    m2 = We.sum(axis=0) / We.shape[0]
    We = np.concatenate([We, m2.reshape(1, m2.size)])
    return We, words

We, words = getWordmap(params.wordfile, params.vocabfile)

if params.load_pickle:
    We = cPickle.load(open(params.load_pickle, 'rb'))
    We = We[0].get_value()

params.vocab = words
model = bag_model(We, params)

print " ".join(sys.argv)
print "Num examples:", len(data)
print "Num words:", len(words)

model.train(data, words, params)
