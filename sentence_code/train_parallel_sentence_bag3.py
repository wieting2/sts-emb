from params import params
import utils
from bag_model import bag_model
import lasagne
import random
import numpy as np
import sys
import argparse
import pdb
from tree2 import tree
import cPickle
from theano import config
from learn_bpe import learn
from apply_bpe import apply
from apply_bpe import BPE
from apply_bpe2 import apply2
from apply_bpe3 import apply3
import copy
import io

f1 = '../data/simple.aligned'
f2 = '../data/normal.aligned'

def get_lines(f):
    f = io.open(f, 'r', encoding='utf-8')
    lines = f.readlines()
    d = []
    for i in lines:
        i = i.split('\t')
        d.append(i[-1].strip().lower())
    return d

def getData(lines):
    examples = []
    for i in lines:
        e = (tree(i[0]), tree(i[1]))
        examples.append(e)
    return examples

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

def str2act(v):
    if v is None:
        return v
    if v.lower() == "tanh":
        return lasagne.nonlinearities.tanh
    if v.lower() == "linear":
        return lasagne.nonlinearities.linear
    if v.lower() == "relu":
        return lasagne.nonlinearities.rectify
    raise ValueError('A type that was supposed to be a learner is not.')

def get_ngrams(examples, size, cutoff):
    d = {}
    for i in examples:
        e1 = " " + i[0].phrase + " "
        e2 = " " + i[1].phrase +" "
        for j in range(len(e1)):
            ngram = e1[j:j+size]
            if len(ngram) != size:
                continue
            if ngram in d:
                d[ngram] += 1
            else:
                d[ngram] = 1
        for j in range(len(e2)):
            ngram = e2[j:j+size]
            if len(ngram) != size:
                continue
            if ngram in d:
                d[ngram] += 1
            else:
                d[ngram] = 1
    feature_map = {}
    idx = 0
    for i in d:
        if d[i] > cutoff:
            feature_map[i] = idx
            idx += 1
    feature_map["UUUNKKK"] = idx
    return feature_map

def get_bpe_ngrams(word, bpe_vocab, o):
    ngrams = []
    for i1 in range(len(word)):
        i2 = i1 + 1
        while i2 <= len(word):
            ngram = word[i1:i2]
            if len(ngram) >= o and ngram in bpe_vocab:
                ngrams.append(ngram)
            i2 += 1
    return ngrams

def get_bpe(examples, params):

    bpe2 = None
    bpe_vocab = None

    if params.bpe_file and params.word_embedding_file:
        bpe2 = apply2(params.bpe_file, examples, params)
    elif params.bpe_file:
        bpe2 = apply3(params.bpe_file, examples, params)
    else:
        bpe1 = learn(examples, params.bpe_size)
        bpe2 = BPE(bpe1, '@@', None, None)
        bpe_vocab = set([])
        for i in bpe2.bpe_codes: bpe_vocab.add("".join(i).replace("</w>", ""))
        apply(bpe1, examples, params, bpe_vocab)

    words = get_words(examples,0)

    return bpe2, words, bpe_vocab

def get_words(examples, cutoff):
    d = {}
    for i in examples:
        e1 = i[0].phrase.split()
        e2 = i[1].phrase.split()
        for j in range(len(e1)):
            wd = e1[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
        for j in range(len(e2)):
            wd = e2[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
    feature_map = {}
    idx = 0
    #pdb.set_trace()
    for i in d:
        if d[i] > cutoff:
            feature_map[i] = idx
            idx += 1
    feature_map["UUUNKKK"] = idx
    return feature_map

data = []

d1 = get_lines(f1)
d2 = get_lines(f2)

for i in range(len(d1)):
    data.append((d1[i],d2[i]))

random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LC", help="Regularization on Composition Parameters", type=float)
parser.add_argument("-LW", help="Regularization on Embedding Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.")
parser.add_argument("-dataf", help="Training data file.")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.", default="MAX")
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.", default="true")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-fraction", help="Percent of training examples to use.", type=float)
parser.add_argument("-clip", help="float to indicate grad cutting. Use 0 for no clipping.",type=int)
parser.add_argument("-eta", help="learning rate",type=float, default=0.001)
parser.add_argument("-learner", help="Either AdaGrad or Adam", default = "adam")
parser.add_argument("-featurefile", help="File containing n-grams and their counts.")
parser.add_argument("-featurefileWord", help="File containing n-grams and their counts.")
parser.add_argument("-featurefilePhrase", help="File containing n-grams and their counts.")
parser.add_argument("-cutoff", help="Above or equal to this, features are kept.", type=int, default=0)
parser.add_argument("-offset", help="n in n-gram for features.", type=int)
parser.add_argument("-layers", help="Number of layers <= 2.", type=int)
parser.add_argument("-act1", help="activation of last layer of DSSM.")
parser.add_argument("-win", help="Number of words to include in context in the left and the right.", type=int)
parser.add_argument("-character_file", help="List of characters for embeddings.")
parser.add_argument("-outgate", help="Outgate")
parser.add_argument("-dropout", help="dropout", type=float)
parser.add_argument("-conv_type", help="conv_type", type=int)
parser.add_argument("-actconv")
parser.add_argument('-noshuffle', default="False")
parser.add_argument('-sum')
parser.add_argument("-model", help="", type=int)
parser.add_argument("-ngramtype", help="", type=int)
parser.add_argument("-charmode", help="")
parser.add_argument("-ls", help="", type=int)
parser.add_argument("-mode", help="")
parser.add_argument("-average", help="")
parser.add_argument("-lam", help="", type=float)
parser.add_argument("-alpha", help="", type=float)
parser.add_argument("-beta", help="", type=float)
parser.add_argument("-dim", help="", type=int)
parser.add_argument("-scramble",type=float, default=0.)
parser.add_argument("-dropout1",type=float, default=0.)
parser.add_argument("-dropout2",type=float, default=0.)
parser.add_argument("-prebatch")
parser.add_argument("-scramble_single")
parser.add_argument("-partial")
parser.add_argument("-model_type",type=int)
parser.add_argument("-sumlayer")
parser.add_argument("-shuffle_exp")
parser.add_argument("-mixed_type")
parser.add_argument("-short")
parser.add_argument("-max",type=int,default=0)
parser.add_argument("-shuffle", default="true")
parser.add_argument("-use_unktoken", default="true")
parser.add_argument("-wordtype", default="word", help = "words, 2grams, 3grams, 4grams, be, beoverlap, wp, wpoverlap")
parser.add_argument("-overlap", default = "false")
parser.add_argument("-load_embeddings", default = None)
parser.add_argument("-bpe_size", type=int)
parser.add_argument("-bpe_overlap", type=int, default=-1)
parser.add_argument("-bpe_file")
parser.add_argument("-bpe_file2")
parser.add_argument("-bpe_ending", type=int, default=1)
parser.add_argument("-use_end_tok", default = "true")
parser.add_argument("-word_embedding_file")

args = parser.parse_args()

params.LC = args.LC
params.LW = args.LW
params.outfile = args.outfile
params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.wordfile = args.wordfile
params.nntype = args.nntype
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.dataf = args.dataf
params.margin = args.margin
params.samplingtype = args.samplingtype
params.epochs = args.epochs
params.evaluate = str2bool(args.evaluate)
params.learner = str2learner(args.learner)
params.eta = args.eta
params.featurefile = args.featurefile
params.featurefileWord = args.featurefileWord
params.featurefilePhrase = args.featurefilePhrase
params.cutoff = args.cutoff
params.offset = args.offset
params.numlayers = args.layers
params.act = str2act(args.act1)
params.nntype = args.nntype
params.win = args.win
params.outgate = str2bool(args.outgate)
params.character_file = args.character_file
params.conv_type = args.conv_type
params.dropout = args.dropout
params.clip = args.clip
#params.outfile = '../models/'+args.outfile
params.actconv = str2act(args.actconv)
params.noshuffle = str2bool(args.noshuffle)
params.sum = str2bool(args.sum)
params.model = args.model
params.nntype = args.nntype
params.ngramtype = args.ngramtype
params.charmode = args.charmode
params.ls = args.ls
params.mode = args.mode
params.average = str2bool(args.average)
params.lam = args.lam
params.alpha = args.alpha
params.beta = args.beta
params.dim = args.dim
params.scramble = args.scramble
params.dropout1 = args.dropout1
params.dropout2 = args.dropout2
params.prebatch = str2bool(args.prebatch)
params.scramble_single = str2bool(args.scramble_single)
params.partial = str2bool(args.partial)
params.model_type = args.model_type
params.sumlayer = str2bool(args.sumlayer)
params.shuffle_exp = str2bool(args.shuffle_exp)
params.mixed_type = str2bool(args.mixed_type)
params.short = str2bool(args.short)
params.max = args.max
params.shuffle = str2bool(args.shuffle)
params.use_unktoken = str2bool(args.use_unktoken)
params.wordtype = args.wordtype
params.overlap = str2bool(args.overlap)
params.load_embeddings = args.load_embeddings
params.bpe_size = args.bpe_size
params.bpe_overlap = args.bpe_overlap
params.bpe_file = args.bpe_file

params.bpe_ending = args.bpe_ending
params.use_end_tok = str2bool(args.use_end_tok)
params.word_embedding_file = args.word_embedding_file

data = getData(data)

if params.mode == "ppdb":
    data = utils.getData("../data/ppdb-XL-ordered-data.txt",None)
    #random.shuffle(d)
    #ct = 0
    #for i in data:
    #    ct += len(i[0].phrase.split()) + len(i[1].phrase.split())
    #data = []
    #print ct
    #idx = 0
    #ct2 = 0
    #while ct > 0:
    #    dd = d[idx]
    #    data.append(dd)
    #    v = len(dd[0].phrase.split()) + len(dd[1].phrase.split())
    #    ct -= v
    #    ct2 += v
    #    idx += 1
    #print ct2

words = None
bpe = None

### DELETE THIS
# d = {}
# for i in data:
#     e1 = i[0].phrase.split()
#     e2 = i[1].phrase.split()
#     for j in range(len(e1)):
#         wd = e1[j]
#         if wd in d:
#             d[wd] += 1
#         else:
#             d[wd] = 1
#     for j in range(len(e2)):
#         wd = e2[j]
#         if wd in d:
#             d[wd] += 1
#         else:
#             d[wd] = 1
###

bpe = None; bpe_vocab = None

if params.word_embedding_file:
    words, We = utils.getWordmap(params.word_embedding_file)
    #if params.wordtype == "words":
    #    pass
    if "grams" in params.wordtype:
        new_words = {}
        for i in words:
            new_words[i.replace("_"," ")] = words[i]
        words = new_words
    words["UUUNKKK"] = len(words)
    #pdb.set_trace()
    We = np.asarray(We, dtype=config.floatX)
    # m2 = lasagne.init.Uniform(1).sample((1, params.dim))
    # We = np.concatenate([We, m2])
    m2 = We.sum(axis=0) / We.shape[0]
    We = np.concatenate([We, m2.reshape(1, m2.size)])

    if params.wordtype == "words":
        words2 = get_words(data, params.cutoff)
    elif params.wordtype == "2grams":
        words2 = get_ngrams(data, 2, params.cutoff)
    elif params.wordtype == "3grams":
        words2 = get_ngrams(data, 3, params.cutoff)
    elif params.wordtype == "4grams":
        words2 = get_ngrams(data, 4, params.cutoff)
    elif params.wordtype == "5grams":
        words2 = get_ngrams(data, 5, params.cutoff)
    elif params.wordtype == "bpe":
        bpe, words2, bpe_vocab = get_bpe(data, params)

    #to be fair, what to do here...get new matrix filled out with words2
    W = lasagne.init.Normal()
    W = W.sample((len(words2), params.dim))
    for i in words2:
        if i in words:
            W[words2[i],:] = We[words[i],:]
    words = words2
    We = W
    #We = (We - We.mean(axis=0)) / We.std(axis=0)
else:
    We = None
    if params.wordtype == "words":
        words = get_words(data, params.cutoff)
    elif params.wordtype == "2grams":
        words = get_ngrams(data, 2, params.cutoff)
    elif params.wordtype == "3grams":
        words = get_ngrams(data, 3, params.cutoff)
    elif params.wordtype == "4grams":
        words = get_ngrams(data, 4, params.cutoff)
    elif params.wordtype == "5grams":
        words = get_ngrams(data, 5, params.cutoff)
    elif params.wordtype == "bpe":
        bpe, words, bpe_vocab = get_bpe(data, params)


#for i in d:
#    if i in words:
#        print i, d[i]

params.vocab = words

if We is not None:
    model = bag_model(params, We)
else:
    model = bag_model(params)

print " ".join(sys.argv)
print "Num examples:", len(data)
print "Num words:", len(words)

#check data
new_data = []
for i in data:
    i[0].populate_embeddings(words, False)
    i[1].populate_embeddings(words, False)
    if len(i[0].embeddings) == 0 or len(i[1].embeddings) == 0:
        #print "removed: {0}\n {1}".format(i[0].phrase, i[1].phrase).encode("utf-8")
        continue
    i[0].unpopulate_embeddings()
    i[1].unpopulate_embeddings()
    new_data.append(i)

data = new_data

model.train(data, words, params, bpe, bpe_vocab)