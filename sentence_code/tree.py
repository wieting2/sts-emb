
import random
import numpy as np
import pdb

def lookup(words,w):
    w = w.lower()
    if w in words:
        return words[w]
    else:
        return words['UUUNKKK']

class tree(object):

    def __init__(self, phrase, words = None):
        self.phrase = phrase.strip()
        self.embeddings = []
        #self.populate_embeddings(words)
        self.representation = None

    def populate_embeddings(self, words):
        phrase = self.phrase.lower()
        arr = phrase.split()
        for i in arr:
            self.embeddings.append(lookup(words,i))

    def populate_embeddings_scramble(self, words):
        phrase = self.phrase.lower()
        arr = phrase.split()
        random.shuffle(arr)
        for i in arr:
            self.embeddings.append(lookup(words,i))

    def populate_embeddings_partial(self, words, lam):
        phrase = self.phrase.lower()
        arr = phrase.split()
        n_swaps = np.random.poisson(lam)
        if len(arr) > 1:
            while n_swaps > 0:
                idx1 = np.random.randint(0,len(arr)-1)
                idx2 = np.random.randint(0,len(arr)-1)
                t = arr[idx1]
                arr[idx1] = arr[idx2]
                arr[idx2] = t
                n_swaps -= 1
        for i in arr:
            self.embeddings.append(lookup(words,i))

    def populate_embeddings_characters(self, chars):
        phrase = " " + self.phrase.lower() + " "
        for i in phrase:
            self.embeddings.append(lookup(chars,i))

    def populate_embeddings_trigrams1(self, chars, shuffle=False):
        phrase = " " + self.phrase.lower() + " "
        for j in range(len(phrase)):
            ngram = phrase[j:j+3]
            if len(ngram) != 3:
                continue
            self.embeddings.append(lookup(chars,ngram))
        if shuffle:
            random.shuffle(self.embeddings)

    def populate_embeddings_trigrams2(self, chars, shuffle=False):
        phrase = " " + self.phrase.lower() + " "
        if len(phrase) % 3 == 1:
            phrase = phrase + " "
        elif len(phrase) % 3 == 2:
            phrase = phrase + "  "
        for j in range(0, len(phrase), 3):
            ngram = phrase[j:j+3]
            self.embeddings.append(lookup(chars,ngram))
        if shuffle:
            random.shuffle(self.embeddings)

    def unpopulate_embeddings(self):
        self.embeddings = []
