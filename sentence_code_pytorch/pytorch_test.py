import numpy as np
from torch import nn
import pdb
from torch.autograd import Variable
import torch

#TODO:
#test uneven batchsizes
#test averaging lstm
#test bidrectional lstm
#test bidirectional averaging lstm
#test bidirectional averaging concat lstm
#test attention

bsz = 200
d_hid = 100
words = np.random.rand(100,50)
data = np.random.randint(0,high=99,size=(200,10))

mask = np.zeros((200,10))
for i in range(200):
    for j in range(np.random.randint(3,high=9)):
        mask[i,j] = 1

word_embs = nn.Embedding(100, 50)
encoder = nn.LSTM(50, 100, num_layers=1, batch_first=True)

input = Variable(torch.from_numpy(data.astype('int32')).long())
mask = Variable(torch.from_numpy(mask.astype('int32')).long())
in_embs = word_embs(input)

e_hid_init = Variable(torch.zeros(1, bsz, d_hid))
e_cell_init = Variable(torch.zeros(1, bsz, d_hid))

enc_hids, (enc_last_hid, enc_last_c) = encoder(in_embs, (e_hid_init, e_cell_init))

#zero out hidden states
out = mask[:,:,None].float() * enc_hids
out = out.sum(dim=1) / mask.sum(dim=1).float()[:,None]

lengths = mask.sum(dim=1)[:,None].data.numpy()
lengths = np.squeeze(lengths)
idx_sort = np.argsort(-lengths)
lengths = lengths[idx_sort]
lengths = np.ndarray.tolist(lengths)
input = input[idx_sort, :]
in_embs = word_embs(input)
packed_embs = nn.utils.rnn.pack_padded_sequence(in_embs, lengths, batch_first = True)

packed_enc_hids, (packed_enc_last_hid, packed_enc_last_c) = encoder(packed_embs, (e_hid_init, e_cell_init))
enc_hids2 = nn.utils.rnn.pad_packed_sequence(packed_enc_hids, batch_first = True)[0]
pdb.set_trace()