from torch import nn
from torch.autograd import Variable
import torch
import numpy as np
import pdb
import time

import sys
import cPickle
import utils

def checkIfQuarter(idx, n):
    # print idx, n
    if idx == round(n / 4.) or idx == round(n / 2.) or idx == round(3 * n / 4.):
        return True
    return False

class lstm_ppdb_model(object):
    # takes list of seqs, puts them in a matrix
    # returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        x_mask = np.asarray(x_mask, dtype=config.floatX)
        return x, x_mask

    def saveParams(self, fname):
        f = file(fname, 'wb')
        cPickle.dump(self.all_params, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
            minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def prepare(self, batch, params):
        #batch is list of tuples
        g1 = []
        g2 = []
        scores = []

        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)
            scores.append(i[2])

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        return (g1x, g1mask, g2x, g2mask, np.asarray(scores, dtype = config.floatX))

    def feedforward_function(self, g, mask):
        if self.dropout1 > 0 or self.dropout2 > 0:
            d = np.ones(g.shape + (300,))
            d = np.asarray(d, dtype=config.floatX)
            return self.feedforward_function_t(g, mask, d)
        else:
            return self.feedforward_function_t(g, mask)

    def scoring_function(self, g1, g2, mask1, mask2):
        if self.dropout1 > 0 or self.dropout2 > 0:
            d1 = np.ones(g1.shape + (300,))
            d2 = np.ones(g2.shape + (300,))
            d1 = np.asarray(d1, dtype=config.floatX)
            d2 = np.asarray(d2, dtype=config.floatX)
            return self.scoring_function_t(g1, g2, mask1, mask2, d1, d2)
        else:
            return self.scoring_function_t(g1, g2, mask1, mask2)

    def __init__(self, We, params):

        self.word_embs = nn.Embedding(We.shape[0], We.shape[1])
        self.word_embs = nn.Parameter(torch.tensor(We))

    def mean_encoder(self, input, mask):
        pass

    def lstm_encoder(self, input, mask, average=False):
        pass

    #output can be lastavg, lastconcat, average, or concat
    def bilstm_encoder(self, input, mask, output="last"):
        pass

    def forward(self, g1, g2, mask1, mask2, args):

        embs1 = self.word_embs(g1)
        embs2 = self.word_embs(g1)

        embg1 = None
        embg2 = None

        if args.nntype == "wordaverage":
            embg1 = self.mean_encoder(embs1, mask1)
            embg2 = self.mean_encoder(embs2, mask2)

        elif args.nntype == "lstm":
            embg1 = self.lstm_encoder(embs1, mask1, average=False)
            embg2 = self.lstm_encoder(embs2, mask2, average=False)

        elif args.nntype == "lstmavg":
            embg1 = self.lstm_encoder(embs1, mask1, average=True)
            embg2 = self.lstm_encoder(embs2, mask2, average=True)

        elif args.nntype == "bilstmlastavg":
            embg1 = self.bilstm_encoder(embs1, mask1, output="lastavg")
            embg2 = self.bilstm_encoder(embs2, mask2, output="lastavg")

        elif args.nntype == "bilstmlastconcat":
            embg1 = self.bilstm_encoder(embs1, mask1, output="lastconcat")
            embg2 = self.bilstm_encoder(embs2, mask2, output="lastconcat")

        elif args.nntype == "bilstmavg":
            embg1 = self.bilstm_encoder(embs1, mask1, output="average")
            embg2 = self.bilstm_encoder(embs2, mask2, output="average")

        elif args.nntype == "bilstmconcat":
            embg1 = self.bilstm_encoder(embs1, mask1, output="concat")
            embg2 = self.bilstm_encoder(embs2, mask2, output="concat")

        if params.loadfile:
            base_params = cPickle.load(open(params.loadfile, 'rb'))
            all_params = lasagne.layers.get_all_params(l_out, trainable=True)

            old_we = base_params.pop(0)
            new_we = all_params.pop(0)

            reg = 0.
            for n in range(len(all_params)):
                reg += lasagne.regularization.l2((all_params[n] - base_params[n]))

            reg = params.LC * reg + params.LW * lasagne.regularization.l2(new_we - old_we)

        self.final_layer = l_out

        embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=False)
        embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=False)

        t_embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=True)
        t_embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=True)

        def fix(x):
            return x*(x > 0) + 1E-10*(x <= 0)

        #objective function
        g1g2 = (embg1*embg2).sum(axis=1)
        g1g2norm = T.sqrt(fix(T.sum(embg1 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg2 ** 2, axis=1)))
        g1g2 = g1g2 / g1g2norm

        cost = (g1g2 - scores)**2
        cost = T.mean(cost)

        self.all_params = lasagne.layers.get_all_params(l_out, trainable=True)

        if not params.loadfile:
            word_reg = 0.5*params.LW*lasagne.regularization.l2(We-initial_We)
            cost = cost + word_reg
        else:
            cost = cost + reg

        #feedforward
        g1g2 = (t_embg1 * t_embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(t_embg1 ** 2, axis=1)) * T.sqrt(T.sum(t_embg2 ** 2, axis=1))
        g1g2 = g1g2 / g1g2norm
        self.feedforward_function = theano.function([g1batchindices,g1mask], t_embg1)
        prediction = g1g2
        self.scoring_function = theano.function([g1batchindices, g2batchindices,
            g1mask, g2mask],prediction)

        print self.all_params
        grads = theano.gradient.grad(cost, self.all_params)
        updates = params.learner(grads, self.all_params, params.eta)

        self.train_function = theano.function([g1batchindices, g2batchindices,
                                                   g1mask, g2mask, scores], cost, updates=updates)

        cost = (g1g2 - scores)**2
        cost = T.mean(cost)
        self.cost_function = theano.function([g1batchindices, g2batchindices,
                                                   g1mask, g2mask, scores], cost)

        print "Num Params:", utils.countParameters(self)


    #trains parameters
    def train(self, train_d, dev_d, test_d, words, params):
        start_time = time.time()
        evaluate(self, words, dev_d, test_d)

        counter = 0
        try:
            for eidx in xrange(params.epochs):

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(train_d), params.batchsize, shuffle=True)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1

                    batch = [train_d[t] for t in train_index]
                    for i in batch:
                        i[0].populate_embeddings(words)
                        i[1].populate_embeddings(words)


                    #(g1x, g1mask, g2x, g2mask, s) = self.prepare(batch, params)
                    (scores, g1x, g1mask, g2x, g2mask) = self.prepare(batch)

                    preds = net(g1, g2, g1mask, g2mask, scores)




                    t1 = time.time()
                    cost = self.train_function(g1x, g2x, g1mask, g2mask, scores)
                    t2 = time.time()
                    print "cost, time: ", cost, t2-t1

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    #undo batch to save RAM
                    for i in batch:
                        i[0].representation = None
                        i[1].representation = None
                        i[0].unpopulate_embeddings()
                        i[1].unpopulate_embeddings()

                        #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                if (params.save):
                    counter += 1
                    self.saveParams(params.outfile + str(counter) + '.pickle')

                if (params.evaluate):
                    evaluate(self, words, dev_d, test_d)

                    if params.shuffle_exp:
                        print "training cost: {0}".format(self.get_cost(params, words))

                print 'Epoch ', (eidx + 1), 'Cost ', cost

        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)