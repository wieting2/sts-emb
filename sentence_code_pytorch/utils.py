from scipy.io import loadmat
import numpy as np
from tree2 import tree
from random import shuffle
import pdb
import time
import lasagne
import theano.tensor as T
import io
from random import randint
from random import choice
from scipy.spatial.distance import cosine
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform

# gets trigrams
def get_ngrams(ln, type):
    features = []
    word = " "+ln.strip()+" "
    for j in range(len(word)):
        idx = j
        gr = ""
        while idx < j + type and idx < len(word):
            gr += word[idx]
            idx += 1
        if not len(gr) == type:
            continue
        features.append(gr)
    return features

def lookup(We,words,w):
    w = w.lower()
    if len(w) > 1 and w[0] == '#':
        w = w.replace("#","")
    if w in words:
        return We[words[w],:]
    else:
        return We[words['UUUNKKK'],:]

def lookupIDX(words,w):
    w = w.lower()
    if len(w) > 1 and w[0] == '#':
        w = w.replace("#","")
    if w in words:
        return words[w]
    else:
        #print w
        return words['UUUNKKK']

def lookupIDX2(words,w):
    w = w.lower()
    if w in words:
        return words[w]
    else:
        #print w                                                                                                                          
        return words['UUUNKKK']

def lookup_with_unk(We,words,w):
    w = w.lower()
    if len(w) > 1 and w[0] == '#':
        w = w.replace("#","")
    if w in words:
        return We[words[w],:],False
    else:
        return We[words['UUUNKKK'],:],True

#changed
def getData(f,words):
    data = io.open(f, 'r', encoding='utf-8')
    lines = data.readlines()
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split('\t')
            if len(i) == 2:
                e = (tree(i[0]), tree(i[1]))
                examples.append(e)
            else:
                print i
    return examples

def getDataDataset(f,words):
    data = open(f,'r')
    lines = data.readlines()
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split('\t')
            if len(i) == 3:
                e = (tree(i[0], words), tree(i[1], words), float(i[2]))
                examples.append(e)
            else:
                print i
    return examples

def getWordmap(textfile):
    words={}
    We = []
    f = open(textfile,'r')
    lines = f.readlines()
    if len(lines[0].split()) == 2:
        lines.pop(0)
    for (n,i) in enumerate(lines):
        i=i.split()
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=n
        print i[0], n
        We.append(v)
    return (words, np.array(We))

def getPairMax(d,idx,v):
    min = -5000
    best = None
    for i in range(len(d)):
        if i == idx:
            continue
        (t1,t2) = d[i]
        v1 = t1.representation
        v2 = t2.representation
        np1 = -cosine(v1,v) + 1
        np2 = -cosine(v2,v) + 1
        if(np1 > min):
            min = np1
            best = t1
        if(np2 > min):
            min = np2
            best = t2
    return best

def getPairRand(d,idx):
    wpick = None
    ww = None
    while(wpick == None or (idx == ww)):
        ww = choice(d)
        ridx = randint(0,1)
        wpick = ww[ridx]
    return wpick

def getPairMix(d,idx,v):
    r1 = randint(0,1)
    if r1 == 1:
        return getPairMax(d,idx,v)
    else:
        return getPairRand(d,idx)

def getPairs(d, type):
    pairs = []
    for i in range(len(d)):
        (t1,t2) = d[i]
        v1 = t1.representation
        v2 = t2.representation
        p1 = None
        p2 = None
        if type == "MAX":
            p1 = getPairMax(d,i,v1)
            p2 = getPairMax(d,i,v2)
        if type == "RAND":
            p1 = getPairRand(d,i)
            p2 = getPairRand(d,i)
        if type == "MIX":
            p1 = getPairMix(d,i,v1)
            p2 = getPairMix(d,i,v2)
        pairs.append((p1,p2))
    return pairs

def getPairsBatch(d, batchsize, type, model):
    idx = 0
    pairs = []
    while idx < len(d):
        batch = d[idx: idx + batchsize if idx + batchsize < len(d) else len(d)]
        if(len(batch) <= 2):
            print "batch too small."
            continue #just move on because pairing could go faulty
        p = getPairs(batch, type, model)
        pairs.extend(p)
        idx += batchsize
    return pairs

def getTuplesFromExamples(d,p):
    d1 = []
    d2 = []
    p1 = []
    p2 = []
    for i in d:
        (t1,t2) = (i[0].embeddings, i[1].embeddings)
        d1.append(t1)
        d2.append(t2)
    for i in p:
        (t1,t2) = (i[0].embeddings, i[1].embeddings)
        p1.append(t1)
        p2.append(t2)
    return d1,d2,p1,p2

def getScores(p1,p2,words,model):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    seqs = [X1]
    x1,m1 = model.prepare_data(seqs)
    seqs = [X2]
    x2,m2 = model.prepare_data(seqs)
    s = model.scoring_function(x1,x2,m1,m2)
    return s

def populateRepresentations(d,words,model):
    X1 = []
    X2 = []
    st = time.time()
    for i in range(len(d)):
        (t1,t2) = d[i]
        p1 = t1.phrase.split()
        p2 = t2.phrase.split()
        x1 = []
        x2 = []
        for i in p1:
            x1.append(lookupIDX(words,i))
        for i in p2:
            x2.append(lookupIDX(words,i))
        X1.append(x1)
        X2.append(x2)
    x1,m1 = model.prepare_data(X1)
    x2,m2 = model.prepare_data(X2)
    rep1 = model.feedforward_function(x1,m1)
    rep2 = model.feedforward_function(x2,m2)
    for i in range(len(d)):
        (t1,t2) = d[i]
        t1.representation = rep1[i,:]
        t2.representation = rep2[i,:]
    ed = time.time()
    #print 'representation time', (ed-st)


def getScoresForBatch(d, model):
    X1 = []
    X2 = []
    T1 = []
    T2 = []
    st = time.time()
    for i in range(len(d)):
        (p1,p2) = d[i]
        X1.append(p1.representation)
        X2.append(p2.representation)
        T1.append(p1)
        T2.append(p2)
    P1f = []
    P2f = []
    treesf = []
    P1s = []
    P2s = []
    treess = []
    pairs = []
    for i in range(len(d)):
        for j in range(len(d)):
            if i == j:
                continue
            P1f.append(X1[i])
            P2f.append(X2[j])
            P1f.append(X1[i])
            P2f.append(X1[j])
            treesf.append(T2[j])
            treesf.append(T1[j])

            P1s.append(X2[i])
            P2s.append(X1[j])
            P1s.append(X2[i])
            P2s.append(X2[j])
            treess.append(T1[j])
            treess.append(T2[j])

        scoresf = model.scoring_function_emb(P1f,P2f)
        scoress = model.scoring_function_emb(P1s,P2s)
        pairs.append((treesf[np.argmax(scoresf)], treess[np.argmax(scoress)]))
        P1f = []
        P2f = []
        treesf = []
        P1s = []
        P2s = []
        treess = []
    ed = time.time()
    return pairs

def getPairMixScore(d,idx,maxpair):
    r1 = randint(0,1)
    if r1 == 1:
        return maxpair
    else:
        return getPairRand(d,idx)

def checkScores(d,maxpairs,model):
    for i in range(len(d)):
        (t1,t2) = d[i]
        (p1,p2) = maxpairs[i]
        print model.scoring_function_emb([t1.representation],[t2.representation])
        #print model.scoring_function_emb([p1.representation],[p2.representation])
        print model.scoring_function_emb([t1.representation],[p1.representation])
        #print model.scoring_function_emb([t2.representation],[p2.representation])
        print model.scoring_function_emb([t1.representation],[d[0][0].representation])
        print ""

def getPairsScore(d, type, words, model):
    pairs = []
    #getScoresForBatch(d, model, words)
    populateRepresentations(d, words, model)
    maxpairs = getScoresForBatch(d, model)
    #checkScores(d,maxpairs,model)
    for i in range(len(d)):
        (t1,t2) = d[i]
        p1 = None
        p2 = None
        if type == "MAX":
            p1 = maxpairs[i][0]
            p2 = maxpairs[i][1]
        if type == "RAND":
            p1 = getPairRand(d,i)
            p2 = getPairRand(d,i)
        if type == "MIX":
            p1 = getPairMixScore(d,i,maxpairs[i][0])
            p2 = getPairMixScore(d,i,maxpairs[i][1])
        pairs.append((p1,p2))
    return pairs

def countParameters(model):
    params = model.all_params
    ct = 0
    for i in params:
        ct += i.get_value().size
    return ct

def analyze_pairs(g1, g2, p1, p2):
    n = g1.shape[0]
    for i in range(n):
        print g1[i,:], g2[i,:], p1[i,:], p2[i,:]

def getPairMixFast(d,idx,t):
    r1 = randint(0,1)
    if r1 == 1:
        return t
    else:
        return getPairRand(d,idx)

def getPairsFast(d, type, delta=None):
    X = []
    T = []
    pairs = []
    for i in range(len(d)):
        (p1,p2) = d[i]
        X.append(p1.representation)
        X.append(p2.representation)
        T.append(p1)
        T.append(p2)

    arr = pdist(X,'cosine')
    arr = squareform(arr)

    # tarr = np.copy(arr)
    #
    # for i in range(len(arr)):
    #     arr[i,i]=1
    #     if i % 2 == 0:
    #         p = arr[i,i+1]
    #         arr[i,i+1] = 1
    #         if delta:
    #             for n,j in enumerate(arr[i]):
    #                 if arr[i,n] <= p + delta:
    #                     arr[i,n] = 1
    #     else:
    #         p = arr[i,i-1]
    #         arr[i,i-1] = 1
    #         if delta:
    #             for n,j in enumerate(arr[i]):
    #                 if arr[i,n] <= p + delta:
    #                     arr[i,n] = 1

    for i in range(len(arr)):
        arr[i,i]=1
        if i % 2 == 0:
            p = arr[i,i+1]
            arr[i,i+1] = 1
            if delta:
                arr[i][np.argwhere(arr[i] <= p + delta)] = 1
        else:
            p = arr[i,i-1]
            arr[i,i-1] = 1
            if delta:
                arr[i][np.argwhere(arr[i] <= p + delta)] = 1

    arr = np.argmin(arr,axis=1)
    for i in range(len(d)):
        (t1,t2) = d[i]
        p1 = None
        p2 = None
        if type == "MAX":
            p1 = T[arr[2*i]]
            p2 = T[arr[2*i+1]]
        if type == "RAND":
            p1 = getPairRand(d,i)
            p2 = getPairRand(d,i)
        if type == "MIX":
            p1 = getPairMixScore(d,i,T[arr[2*i]])
            p2 = getPairMixScore(d,i,T[arr[2*i+1]])
        pairs.append((p1,p2))
    return pairs

def lstm_step2(xt, ctm1, htm1, W):

    l = W.shape[0]
    xt = xt.dimshuffle(1,0)
    ctm1 = ctm1.dimshuffle(1,0)
    htm1 = htm1.dimshuffle(1,0)

    X = T.concatenate((xt,htm1))
    #print X.tag.test_value.shape
    #print W.get_value().shape
    ifoa_linear = T.dot(W,X)
    ifo_gate = lasagne.nonlinearities.sigmoid(ifoa_linear[0:3*l/4,:])
    i_gate = ifo_gate[0:l/4,:]
    f_gate = ifo_gate[l/4:l/2,:]
    o_gate = ifo_gate[l/2:3*l/4,:]
    a_signal = lasagne.nonlinearities.tanh(ifoa_linear[3*l/4:l,:])

    #print f_gate.tag.test_value
    #print ctm1.tag.test_value.shape

    ct = f_gate * ctm1 + i_gate*a_signal
    ht = o_gate * lasagne.nonlinearities.tanh(ct)

    ct = ct.dimshuffle(1,0)
    ht = ht.dimshuffle(1,0)

    return ht, ct
