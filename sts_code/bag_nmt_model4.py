import theano
import numpy as np
from theano import tensor as T
from theano import config
import codecs
import pdb
from evaluate_reg import evaluate
import time
import utils
from lstm_average_layer import lstm_average_layer
from lasagne_average_layer import lasagne_average_layer
from lasagne_average_layer2 import lasagne_average_layer2
from lasagne_max_layer2 import lasagne_max_layer2
from lasagne_max_layer import lasagne_max_layer
from lstm_average_layer_no_output_gate import lstm_average_layer_no_output_gate
from lasagne_cleanse_layer import lasagne_cleanse_layer
import lasagne
import sys
import cPickle

#add bilstm avg and bilstm max pool

def checkIfQuarter(lo, to, n):
    # print idx, n
    while lo < to:
        if lo == round(n / 4.) or lo == round(n / 2.) or lo == round(3 * n / 4.):
            return True
        lo += 1
    return False

class bag_nmt_model(object):

    # takes list of seqs, puts them in a matrix
    # returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        x_mask = np.asarray(x_mask, dtype=config.floatX)
        return x, x_mask

    def prepare(self, batch):
        g1 = []
        g2 = []
        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        scores = []
        for i in batch:
            temp = np.zeros(self.nout)
            score = float(i[2])
            ceil, fl = int(np.ceil(score)), int(np.floor(score))
            if ceil == fl:
                temp[fl - 1] = 1
            else:
                temp[fl - 1] = ceil - score
                temp[ceil - 1] = score - fl
            scores.append(temp)
        scores = np.matrix(scores) + 0.000001
        scores = np.asarray(scores, dtype=config.floatX)
        return (scores, g1x, g1mask, g2x, g2mask)

    def saveParams(self, fname):
        f = file(fname, 'wb')
        values = lasagne.layers.get_all_param_values(self.final_layer)
        #values = [i.get_value() for i in values]
        cPickle.dump(values, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def saveParams2(self, fname, words):
        f = codecs.open(fname, 'w', 'utf-8')
        values = lasagne.layers.get_all_param_values(self.layer)
        for i in words:
            wd = i.replace(" ","@@_")
            v = values[0][words[i],:]
            s = wd+" "
            for i in v:
                s += str(i)+" "
            s = s.strip()
            f.write(s+"\n")
        f.close()

    def saveParams3(self, fname, words):
        f = file(fname, 'wb')
        values = lasagne.layers.get_all_param_values(self.layer)
        #values = [i.get_value() for i in values]
        values.append(words)
        cPickle.dump(values, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
            minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getpairs(self, batch, params):
        #batch is list of tuples
        g1 = []
        g2 = []

        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1_ = [g1[i:i + params.batchsize] for i in xrange(0, len(g1), params.batchsize)]
        g2_ = [g2[i:i + params.batchsize] for i in xrange(0, len(g2), params.batchsize)]
        embg1 = []
        embg2 = []
        for i in range(len(g1_)):
            g1x, g1mask = self.prepare_data(g1_[i])
            g2x, g2mask = self.prepare_data(g2_[i])

            embg1_ = self.feedforward_function(g1x, g1mask)
            embg2_ = self.feedforward_function(g2x, g2mask)
            embg1.append(embg1_)
            embg2.append(embg2_)
        embg1 = np.vstack(embg1)
        embg2 = np.vstack(embg2)

        #update representations
        for idx, i in enumerate(batch):
            i[0].representation = embg1[idx, :]
            i[1].representation = embg2[idx, :]

        #pairs = utils.getPairs(batch, params.type)
        pairs = utils.getPairsFast(batch, params.type, params.delta)
        p1 = []
        p2 = []
        for i in pairs:
            p1.append(i[0].embeddings)
            p2.append(i[1].embeddings)

        p1x, p1mask = self.prepare_data(p1)
        p2x, p2mask = self.prepare_data(p2)

        #for n,i in enumerate(pairs):
        #    print batch[n][0].phrase, pairs[n][0].phrase
        #    print batch[n][1].phrase, pairs[n][1].phrase

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)
        return (g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask)

    def __init__(self, We_initial, params):

        initial_We = theano.shared(np.asarray(We_initial, dtype=config.floatX))
        We = theano.shared(np.asarray(We_initial, dtype=config.floatX))

        self.nout = 6

        #symbolic params
        g1batchindices = T.imatrix()
        g2batchindices = T.imatrix()
        g1mask = T.matrix()
        g2mask = T.matrix()
        scores = T.matrix()

        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)

        if params.dropout > 0:
            l_emb = lasagne.layers.DropoutLayer(l_emb, params.dropout)

        if params.nntype == "gran":
            #if params.outgate:
            if True:
                l_lstm = lstm_average_layer(l_emb, params.dim, peepholes=True, learn_init=False,
                                              mask_input=l_mask, model_type=1)
            else:
                l_lstm = lstm_average_layer_no_output_gate(l_emb, params.dim, peepholes=True, learn_init=False,
                                              mask_input=l_mask, model_type=1)

            if params.model_type == 1 or params.model_type == 2:
                l_out = lasagne_average_layer([l_lstm, l_mask], tosum = False)
            else:
                l_out = lasagne.layers.SliceLayer(l_lstm, -1, 1)

        elif params.nntype == "granmax":

            l_lstm = lstm_average_layer(l_emb, params.dim, peepholes=True, learn_init=False,
                                              mask_input=l_mask, model_type=1)

            l_out = lasagne_max_layer([l_lstm, l_mask], params)


        elif params.nntype == "wordaverage":
            l_out = lasagne_average_layer([l_emb, l_mask], tosum=False)

        elif params.nntype == "maxpool":
            l_out = lasagne_max_layer([l_emb, l_mask], params)

        elif params.nntype == "lstmavg":
            l_lstm = lasagne.layers.LSTMLayer(l_emb, params.dim, peepholes=True, learn_init=False,
                                              mask_input=l_mask)

            l_out = lasagne_average_layer([l_lstm, l_mask], tosum = False)

        elif params.nntype == "lstmmax":
            l_lstm = lasagne.layers.LSTMLayer(l_emb, params.dim, peepholes=True, learn_init=False,
                                              mask_input=l_mask)

            l_out = lasagne_max_layer([l_lstm, l_mask], params)

        elif params.nntype == "bilstmavg":
            l_lstm = lasagne.layers.LSTMLayer(l_emb, params.dim, peepholes=True, learn_init=False,
                                              mask_input=l_mask)

            l_lstmb = lasagne.layers.LSTMLayer(l_emb, params.dim, learn_init=False,
                                               mask_input=l_mask, backwards=True)

            l_cleanse = lasagne_cleanse_layer([l_lstm, l_mask], to_pool = False)
            l_cleanse_b = lasagne_cleanse_layer([l_lstmb, l_mask], to_pool = False)
            l_concat = lasagne.layers.ConcatLayer([l_cleanse, l_cleanse_b], axis=params.axis)
            l_out = lasagne_average_layer2([l_concat, l_mask])

        elif params.nntype == "bilstmmax":
            l_lstm = lasagne.layers.LSTMLayer(l_emb, params.dim, peepholes=True, learn_init=False,
                                              mask_input=l_mask)

            l_lstmb = lasagne.layers.LSTMLayer(l_emb, params.dim, learn_init=False,
                                               mask_input=l_mask, backwards=True)

            l_cleanse = lasagne_cleanse_layer([l_lstm, l_mask], to_pool = True)
            l_cleanse_b = lasagne_cleanse_layer([l_lstmb, l_mask], to_pool = True)
            l_concat = lasagne.layers.ConcatLayer([l_cleanse, l_cleanse_b], axis=params.axis)
            l_out = lasagne_max_layer2([l_concat])

        if params.loadfile:
            base_params = cPickle.load(open(params.loadfile, 'rb'))
            all_params = lasagne.layers.get_all_params(l_out, trainable=True)

            old_we = base_params.pop(0)
            new_we = all_params.pop(0)

            reg = 0.
            for n in range(len(all_params)):
                reg += lasagne.regularization.l2((all_params[n] - base_params[n]))

            reg = params.LC * reg + params.LW * lasagne.regularization.l2(new_we - old_we)

        self.final_layer = l_out

        embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=False)
        embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=False)

        t_embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=True)
        t_embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=True)

        def fix(x):
            return x*(x > 0) + 1E-10*(x <= 0)

        #objective function
        g1g2 = (embg1*embg2).sum(axis=1)
        g1g2norm = T.sqrt(fix(T.sum(embg1 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg2 ** 2, axis=1)))
        g1g2 = g1g2 / g1g2norm

        g1_dot_g2 = embg1*embg2
        g1_abs_g2 = abs(embg1-embg2)

        t_g1_dot_g2 = t_embg1*t_embg2
        t_g1_abs_g2 = abs(t_embg1-t_embg2)

        if params.axis == 1:
            l_in_dot = lasagne.layers.InputLayer((None, params.dim))
            l_in_abs = lasagne.layers.InputLayer((None, params.dim))
        else:
            l_in_dot = lasagne.layers.InputLayer((None, params.dim * 2))
            l_in_abs = lasagne.layers.InputLayer((None, params.dim * 2))

        l_sum = lasagne.layers.ConcatLayer([l_in_dot, l_in_abs])
        l_sigmoid = lasagne.layers.DenseLayer(l_sum, 512, nonlinearity=lasagne.nonlinearities.tanh)

        l_softmax = lasagne.layers.DenseLayer(l_sigmoid, self.nout, nonlinearity=T.nnet.softmax)
        X = lasagne.layers.get_output(l_softmax, {l_in_dot:g1_dot_g2, l_in_abs:g1_abs_g2})
        Y = T.log(X)

        t_X = lasagne.layers.get_output(l_softmax, {l_in_dot:t_g1_dot_g2, l_in_abs:t_g1_abs_g2})

        cost = scores*(T.log(scores) - Y)
        cost = cost.sum(axis=1)/(float(self.nout))

        prediction = 0.
        i = params.minval
        while i<= params.maxval:
            prediction = prediction + i*t_X[:,i-1]
            i += 1

        self.all_params = lasagne.layers.get_all_params([l_out, l_softmax], trainable=True)

        word_reg = 0.5*params.LW*lasagne.regularization.l2(We-initial_We)
        cost = T.mean(cost) + word_reg

        #feedforward
        g1g2 = (t_embg1 * t_embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(t_embg1 ** 2, axis=1)) * T.sqrt(T.sum(t_embg2 ** 2, axis=1))
        g1g2 = g1g2 / g1g2norm
        self.feedforward_function = theano.function([g1batchindices,g1mask], t_embg1)
        self.scoring_function = theano.function([g1batchindices, g2batchindices,
            g1mask, g2mask],prediction)

        print self.all_params
        grads = theano.gradient.grad(cost, self.all_params)
        updates = params.learner(grads, self.all_params, params.eta)

        self.train_function = theano.function([g1batchindices, g2batchindices,
                                                   g1mask, g2mask, scores], cost, updates=updates)

        print "Num Params:", utils.countParameters(self)


    #trains parameters
    def train(self, train_d, dev_d, test_d, words, params):
        start_time = time.time()
        evaluate(self, words, dev_d, test_d)

        v = 0
        counter = 0
        try:
            for eidx in xrange(params.epochs):

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(train_d), params.batchsize, shuffle=True)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1

                    batch = [train_d[t] for t in train_index]
                    for i in batch:
                        i[0].populate_embeddings(words, True)
                        i[1].populate_embeddings(words, True)

                    #(g1x, g1mask, g2x, g2mask, s) = self.prepare(batch, params)
                    (scores, g1x, g1mask, g2x, g2mask) = self.prepare(batch)

                    t1 = time.time()
                    cost = self.train_function(g1x, g2x, g1mask, g2mask, scores)
                    t2 = time.time()
                    print "cost, time: ", cost, t2-t1

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    #undo batch to save RAM
                    for i in batch:
                        i[0].representation = None
                        i[1].representation = None
                        i[0].unpopulate_embeddings()
                        i[1].unpopulate_embeddings()

                if (params.evaluate):
                    dev, tst = evaluate(self, words, dev_d, test_d)
                    if dev > v:
                        v = dev
                        print dev, tst
                        self.saveParams(params.outfile + str(counter) + '.pickle')

                print 'Epoch ', (eidx + 1), 'Cost ', cost

        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)
