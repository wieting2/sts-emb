
import lasagne
from theano import tensor as T

class lasagne_cleanse_layer(lasagne.layers.MergeLayer):

    def __init__(self, incoming, to_pool = False, **kwargs):
        super(lasagne_cleanse_layer, self).__init__(incoming, **kwargs)
        self.to_pool = to_pool

    def get_output_for(self, inputs, **kwargs):
        emb = inputs[0]
        mask = inputs[1]
        emb = (emb * mask[:, :, None])
        if self.to_pool:
            emb += ((mask - 1) * 100)[:, :, None]
        return emb

    def get_output_shape_for(self, input_shape):
        return input_shape[0]