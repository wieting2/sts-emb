
import lasagne
import numpy as np
from theano import tensor as T

class lasagne_dense_layer2(lasagne.layers.Layer):
    def __init__(self, incoming, num_units, num_inputs, W=lasagne.init.GlorotUniform(),
                 nonlinearity=lasagne.nonlinearities.rectify, **kwargs):

        super(lasagne_dense_layer2, self).__init__(incoming, **kwargs)
        self.nonlinearity = nonlinearity
        self.num_units = num_units
        self.W = self.add_param(W, (num_units, num_inputs), name="W")

    def get_output_shape_for(self, input_shape):
        return (self.num_units, input_shape[1])

    def get_output_for(self, input, **kwargs):
        activation = T.dot(self.W, input)
        activation = activation.dimshuffle(1,0)
        activation = self.nonlinearity(activation)
        return activation.dimshuffle(1,0)