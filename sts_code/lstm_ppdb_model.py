import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
from evaluate_reg import evaluate_all
import time
import utils
from LSTMLayerNoOutput import LSTMLayerNoOutput
from drop_layer import drop_layer
from lstm_average_layer import lstm_average_layer
from lasagne_sum_layer import lasagne_sum_layer
from gated_averaging_layer import gated_averaging_layer
from lasagne_average_layer import lasagne_average_layer
from lstm_average_layer_no_output_gate import lstm_average_layer_no_output_gate
import lasagne
import sys
import cPickle

def checkIfQuarter(idx, n):
    # print idx, n
    if idx == round(n / 4.) or idx == round(n / 2.) or idx == round(3 * n / 4.):
        return True
    return False

class lstm_ppdb_model(object):

    # takes list of seqs, puts them in a matrix
    # returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        x_mask = np.asarray(x_mask, dtype=config.floatX)
        return x, x_mask

    def saveParams(self, fname):
        f = file(fname, 'wb')
        values = lasagne.layers.get_all_param_values(self.layer)
        #values = [i.get_value() for i in values]
        cPickle.dump(values, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
            minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getpairs(self, batch, params):
        #batch is list of tuples
        g1 = []
        g2 = []

        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        embg1 = self.feedforward_function(g1x, g1mask)
        embg2 = self.feedforward_function(g2x, g2mask)

        #update representations
        for idx, i in enumerate(batch):
            i[0].representation = embg1[idx, :]
            i[1].representation = embg2[idx, :]

        #pairs = utils.getPairs(batch, params.type)
        pairs = utils.getPairsFast(batch, params.type)
        p1 = []
        p2 = []
        for i in pairs:
            p1.append(i[0].embeddings)
            p2.append(i[1].embeddings)

        p1x, p1mask = self.prepare_data(p1)
        p2x, p2mask = self.prepare_data(p2)

        #for n,i in enumerate(pairs):
        #    print batch[n][0].phrase, pairs[n][0].phrase
        #    print batch[n][1].phrase, pairs[n][1].phrase

        return (g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask)

    def __init__(self, We_initial, params, features=None):

        #params
        if not features:
            initial_We = theano.shared(np.asarray(We_initial, dtype=config.floatX))
            We = theano.shared(np.asarray(We_initial, dtype=config.floatX))
        
        self.dropout1 = params.dropout1
        self.dropout2 = params.dropout2

        #symbolic params
        g1batchindices = T.imatrix()
        g2batchindices = T.imatrix()
        p1batchindices = T.imatrix()
        p2batchindices = T.imatrix()
        g1mask = T.matrix()
        g2mask = T.matrix()
        p1mask = T.matrix()
        p2mask = T.matrix()

        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        if not features:
            l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)
        else:
            l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=len(features), output_size=300)

        if params.dropout1 > 0:
            l_emb = lasagne.layers.DropoutLayer(l_emb, params.dropout1)
        elif params.dropout2 > 0:
            l_emb = lasagne.layers.DropoutLayer(l_emb, params.dropout2, shared_axes=(2,))

        if params.nntype == "lstm":
            if params.outgate:
                l_lstm = lasagne.layers.LSTMLayer(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask)
            else:
                l_lstm = LSTMLayerNoOutput(l_emb, 300, peepholes=True, learn_init=False,
                                       mask_input=l_mask)
            l_out = lasagne.layers.SliceLayer(l_lstm, -1, 1)

        elif params.nntype == "lstmavg":
            if params.outgate:
                l_lstm = lasagne.layers.LSTMLayer(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask)
            else:
                l_lstm = LSTMLayerNoOutput(l_emb, 300, peepholes=True, learn_init=False,
                                       mask_input=l_mask)

            l_out = lasagne_average_layer([l_lstm, l_mask], tosum = params.sum)

        elif params.nntype == "newlstmavg":
            if params.outgate:
                l_lstm = lstm_average_layer(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask, model_type=params.model_type)
            else:
                l_lstm = lstm_average_layer_no_output_gate(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask, model_type=params.model_type)

            if params.model_type == 1 or params.model_type == 2:
                l_out = lasagne_average_layer([l_lstm, l_mask], tosum = params.sum)
            else:
                l_out = lasagne.layers.SliceLayer(l_lstm, -1, 1)

        elif params.nntype == "binewlstmavg":
            if params.outgate:
                l_lstm = lstm_average_layer(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask, model_type=params.model_type)
                l_lstmb = lstm_average_layer(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask, backwards=True)
            else:
                l_lstm = lstm_average_layer_no_output_gate(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask, model_type=params.model_type)
                l_lstmb = lstm_average_layer_no_output_gate(l_emb, 300, peepholes=True, learn_init=False,
                                              mask_input=l_mask, backwards=True)

            if not params.sumlayer:
                l_concat = lasagne.layers.ConcatLayer([l_lstm, l_lstmb], axis=2)
                l_out = lasagne.layers.DenseLayer(l_concat, 300, num_leading_axes=-1, nonlinearity = lasagne.nonlinearities.tanh)
                l_out = lasagne_average_layer([l_out, l_mask], tosum = params.sum)
            else:
                l_out = lasagne_sum_layer([l_lstm, l_lstmb])
                l_out = lasagne_average_layer([l_out, l_mask], tosum = params.sum)

        elif params.nntype == "bilstmavg":
            if params.outgate:
                l_rnn = lasagne.layers.LSTMLayer(l_emb, 300, learn_init=False,
                                              mask_input=l_mask)
                l_rnnb = lasagne.layers.LSTMLayer(l_emb, 300, learn_init=False,
                                                  mask_input=l_mask, backwards=True)
            else:
                l_rnn = LSTMLayerNoOutput(l_emb, 300, learn_init=False,
                                       mask_input=l_mask)
                l_rnnb = LSTMLayerNoOutput(l_emb, 300, learn_init=False,
                                           mask_input=l_mask, backwards=True)

            if not params.sumlayer:
                l_concat = lasagne.layers.ConcatLayer([l_rnn, l_rnnb], axis=2)
                l_out = lasagne.layers.DenseLayer(l_concat, 300, num_leading_axes=-1, nonlinearity = lasagne.nonlinearities.tanh)
                l_out = lasagne_average_layer([l_out, l_mask], tosum = params.sum)
            else:
                l_out = lasagne_sum_layer([l_rnn, l_rnnb])
                l_out = lasagne_average_layer([l_out, l_mask], tosum = params.sum)

        elif params.nntype == "bilstm":
            if params.outgate:
                l_rnn = lasagne.layers.LSTMLayer(l_emb, 300, learn_init=False,
                                              mask_input=l_mask)
                l_rnnb = lasagne.layers.LSTMLayer(l_emb, 300, learn_init=False,
                                                  mask_input=l_mask, backwards=True)
            else:
                l_rnn = LSTMLayerNoOutput(l_emb, 300, learn_init=False,
                                       mask_input=l_mask)
                l_rnnb = LSTMLayerNoOutput(l_emb, 300, learn_init=False,
                                           mask_input=l_mask, backwards=True)

            if not params.sumlayer:
                l_outf = lasagne.layers.SliceLayer(l_rnn, -1, 1)
                l_outb = lasagne.layers.SliceLayer(l_rnnb, -1, 1)
            
                l_concat = lasagne.layers.ConcatLayer([l_outf, l_outb], axis=1)
                l_out = lasagne.layers.DenseLayer(l_concat, 300, nonlinearity = lasagne.nonlinearities.tanh)
            else:
                l_out = lasagne_sum_layer([l_rnn, l_rnnb])
                l_out = lasagne.layers.DenseLayer(l_out, 300, nonlinearity = lasagne.nonlinearities.tanh)

        elif params.nntype == "average":
            l_rnn = gated_averaging_layer(l_emb, 300,
                                          mask_input=l_mask, model_type = params.model_type)

            l_out = lasagne.layers.SliceLayer(l_rnn, -1, 1)

        elif params.nntype == "averageavg":
            l_rnn = gated_averaging_layer(l_emb, 300,
                                          mask_input=l_mask, model_type = params.model_type)

            l_out = lasagne_average_layer([l_rnn, l_mask], tosum = params.sum)

        elif params.nntype == "wordaverage":
            l_out = lasagne_average_layer([l_emb, l_mask], tosum=False)

        self.final_layer = l_out
        embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=False)
        embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=False)
        embp1 = lasagne.layers.get_output(l_out, {l_in: p1batchindices, l_mask: p1mask}, deterministic=False)
        embp2 = lasagne.layers.get_output(l_out, {l_in: p2batchindices, l_mask: p2mask}, deterministic=False)

        t_embg1 = lasagne.layers.get_output(l_out, {l_in: g1batchindices, l_mask: g1mask}, deterministic=True)
        t_embg2 = lasagne.layers.get_output(l_out, {l_in: g2batchindices, l_mask: g2mask}, deterministic=True)

        def fix(x):
            return x*(x > 0) + 1E-10*(x <= 0)

        #objective function
        g1g2 = (embg1 * embg2).sum(axis=1)
        g1g2norm = T.sqrt(fix(T.sum(embg1 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg2 ** 2, axis=1)))
        g1g2 = g1g2 / g1g2norm

        p1g1 = (embp1 * embg1).sum(axis=1)
        p1g1norm = T.sqrt(fix(T.sum(embp1 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg1 ** 2, axis=1)))
        p1g1 = p1g1 / p1g1norm

        p2g2 = (embp2 * embg2).sum(axis=1)
        p2g2norm = T.sqrt(fix(T.sum(embp2 ** 2, axis=1))) * T.sqrt(fix(T.sum(embg2 ** 2, axis=1)))
        p2g2 = p2g2 / p2g2norm

        costp1g1 = params.margin - g1g2 + p1g1
        costp1g1 = costp1g1 * (costp1g1 > 0)

        costp2g2 = params.margin - g1g2 + p2g2
        costp2g2 = costp2g2 * (costp2g2 > 0)

        cost = costp1g1 + costp2g2
        network_params = lasagne.layers.get_all_params(l_out, trainable=True)
        network_params.pop(0)
        self.all_params = lasagne.layers.get_all_params(l_out, trainable=True)
        self.layer = l_out
        print self.all_params

        #regularization
        l2 = 0.5 * params.LC * sum(lasagne.regularization.l2(x) for x in network_params)
        if not params.tri:
            word_reg = 0.5 * params.LW * lasagne.regularization.l2(We - initial_We)
            cost = T.mean(cost) + l2 + word_reg
        else:
            cost = T.mean(cost) + l2

        g1g2 = (t_embg1 * t_embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(t_embg1 ** 2, axis=1)) * T.sqrt(T.sum(t_embg2 ** 2, axis=1))
        g1g2 = g1g2 / g1g2norm
        self.feedforward_function = theano.function([g1batchindices,g1mask], t_embg1)
        prediction = g1g2
        self.scoring_function = theano.function([g1batchindices, g2batchindices,
            g1mask, g2mask],prediction)

        #updates
        grads = theano.gradient.grad(cost, self.all_params)
        updates = params.learner(grads, self.all_params, params.eta)

        self.train_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                                                  g1mask, g2mask, p1mask, p2mask], cost, updates=updates)

        cost = costp1g1 + costp2g2
        cost = T.mean(cost)
        self.cost_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                                                  g1mask, g2mask, p1mask, p2mask], cost)

        print "Num Params:", utils.countParameters(self)

    def scramble(self, t, params, words):
        if not params.lam:
            t.populate_embeddings_scramble(words)
        else:
            t.populate_embeddings_partial(words, params.lam)

    def get_cost(self, params, words):
        kf = self.get_minibatches_idx(len(params.eval_data), params.batchsize, shuffle=True)
        tc = 0
        for _, train_index in kf:

            batch = [params.eval_data[t] for t in train_index]
            for i in batch:
                i[0].populate_embeddings(words)
                i[1].populate_embeddings(words)

            (g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask) = self.getpairs(batch, params)
            cost = self.cost_function(g1x, g2x, p1x, p2x, g1mask, g2mask, p1mask, p2mask)
            tc += cost
        return tc

    #trains parameters
    def train(self, data, words, params):
        if params.mixed_type:
            print "MIXED TYPE"
            params.type = "RAND"

        start_time = time.time()
        evaluate_all(self,words,params)
        if params.shuffle_exp:
            print "training cost: {0}".format(self.get_cost(params, words))

        counter = 0
        try:
            print params.epochs
            for eidx in xrange(params.epochs):

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(data), params.batchsize, shuffle=params.shuffle)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1

                    batch = [data[t] for t in train_index]

                    if (params.scramble or params.lam) and params.prebatch:
                        for i in batch:
                            i[0].populate_embeddings(words)
                            i[1].populate_embeddings(words)
                        (g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask) = self.getpairs(batch, params)

                    for i in batch:
                        if params.tri:
                            i[0].populate_embeddings_trigrams1(words, shuffle=params.scramble)
                            i[1].populate_embeddings_trigrams1(words, shuffle=params.scramble)
                        else:
                            if params.scramble or params.lam:
                                if params.scramble_single:
                                    n = np.random.binomial(1,params.scramble,1)[0]
                                    if n > 0:
                                        self.scramble(i[0], params, words)
                                        self.scramble(i[1], params, words)
                                    else:
                                        i[0].populate_embeddings(words)
                                        i[1].populate_embeddings(words)
                                else:
                                    n1 = np.random.binomial(1,params.scramble,1)[0]
                                    if n1 > 0:
                                        self.scramble(i[0], params, words)
                                    else:
                                        i[0].populate_embeddings(words)
                                    n2 = np.random.binomial(1,params.scramble,1)[0]
                                    if n2 > 0:
                                        self.scramble(i[1], params, words)
                                    else:
                                        i[1].populate_embeddings(words)
                            else:
                                i[0].populate_embeddings(words)
                                i[1].populate_embeddings(words)

                    if not (params.scramble or params.lam) or not params.prebatch:
                        (g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask) = self.getpairs(batch, params)

                    t1 = time.time()
                    cost = self.train_function(g1x, g2x, p1x, p2x, g1mask, g2mask, p1mask, p2mask)
                    t2 = time.time()
                    print "cost, time: ", cost, t2-t1

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    if (checkIfQuarter(uidx, len(kf))):
                        if (params.save):
                            counter += 1
                            self.saveParams(params.outfile + str(counter) + '.pickle')
                        if (params.evaluate):
                            evaluate_all(self, words, params)
                            sys.stdout.flush()

                    #undo batch to save RAM
                    for i in batch:
                        i[0].representation = None
                        i[1].representation = None
                        i[0].unpopulate_embeddings()
                        i[1].unpopulate_embeddings()

                        #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                if (params.save):
                    counter += 1
                    self.saveParams(params.outfile + str(counter) + '.pickle')

                if (params.evaluate):
                    evaluate_all(self, words, params)
                    if params.shuffle_exp:
                        print "training cost: {0}".format(self.get_cost(params, words))

                if params.mixed_type:
                    print "MIXED TYPE"
                    params.type = "MAX"

                print 'Epoch ', (eidx + 1), 'Cost ', cost

        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)
