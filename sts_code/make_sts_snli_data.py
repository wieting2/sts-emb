import random

import numpy as np
from params import params

from to_release.main.example import example


def get_data2(f):
    f = open(f,'r')
    lines = f.readlines()
    #lines = lines[0:int(len(lines)*0.5)]
    data = []
    for i in lines:
        i = i.split("\t")
        p1 = i[0].strip(); p2 = i[1].strip(); score = float(i[2])
        score = score + 1
        #score = -1 + score * .4
        data.append((example(p1, None), example(p2, None), score))
    return data

random.seed(1)
np.random.seed(1)

prefix = "../datasets_tokenized/"
farr = []

farr = ["FNWN2013",#0
        "MSRpar2012",#2
        #"MSRvid2012",#3
        "OnWN2012",#4
        "OnWN2013",#5
        "OnWN2014",#6
        "SMT2013",#7
        "SMTeuro2012",#8
        "SMTnews2012",#9
        "answer-forum2015",#12
        "answer-student2015",#13
        "belief2015",#14
        "deft-forum2014",#18
        "deft-news2014",#19
        "headline2013",#20
        "headline2014",#21
        "headline2015",#22
        "images2014",#23
        "images2015",#24
        "tweet-news2014",
        "answer2016",
        "headlines2016",
        "plagarism2016",
        "postediting2016",
        "question2016"
        ]#27

params.minval = 1
params.maxval = 6
data = []
train_d = []
#dev_d = []
test_d = []
for i in farr:
    d = get_data2(prefix + i)
    idx1 = int(len(d)*0.8)
    #idx2 = int(len(d)*0.1)
    data.extend(d)
    train_d.extend(d[0:idx1])
    #dev_d.extend(d[idx1:idx1+idx2])
    test_d.extend(d[idx1::])


