
from bag_nmt_model4 import bag_nmt_model
import lasagne
import random
import numpy as np
import sys
import argparse
import pdb
from tree2 import tree
import io
from collections import defaultdict


def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

def str2act(v):
    if v is None:
        return v
    if v.lower() == "tanh":
        return lasagne.nonlinearities.tanh
    if v.lower() == "linear":
        return lasagne.nonlinearities.linear
    if v.lower() == "relu":
        return lasagne.nonlinearities.rectify
    raise ValueError('A type that was supposed to be a learner is not.')

def get_data2(f):
    f = open(f,'r')
    lines = f.readlines()
    #lines = lines[0:int(len(lines)*0.5)]
    data = []
    for i in lines:
        i = i.split("\t")
        p1 = i[0].strip(); p2 = i[1].strip(); score = float(i[2])
        score = score + 1
        #score = -1 + score * .4
        data.append((tree(p1), tree(p2), score))
    return data

random.seed(1)
np.random.seed(1)

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Embedding Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-dim", help="Size of input", type=int, default=300)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-vocabfile", help="Word file to be read in.")
parser.add_argument("-save", help="Whether to pickle the model.", default="false")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.", default="MAX")
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.", default="True")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-eta", help="learning rate", type=float, default=0.001)
parser.add_argument("-learner", help="Either AdaGrad or Adam", default="Adam")
parser.add_argument("-outgate", help="Outgate")
parser.add_argument("-dropout", help="dropout", type=float, default=0.)
parser.add_argument("-scramble", help="scramble", type=float, default=0.)
parser.add_argument("-model", help="", type=int)
parser.add_argument("-mode", help="", default="sentence")
parser.add_argument("-model_type", type=int, default=1)
parser.add_argument("-max", type=int, default=0)
parser.add_argument("-load_pickle")
parser.add_argument("-min_value", type=float, default = 0.)
parser.add_argument("-max_value", type=float, default = 1.)
parser.add_argument("-num_examples", type=int, default=200000)
parser.add_argument("-dataf")
parser.add_argument("-wordtype")
parser.add_argument("-mb_batchsize", type=int, default=4)
parser.add_argument("-delta", type=float, default=0)
parser.add_argument("-axis", type=int, default=1)
parser.add_argument("-random", default="False")
parser.add_argument("-data2", default="False")
parser.add_argument("-loadfile", type=int, default=0)

args = parser.parse_args()

args.save = str2bool(args.save)
args.evaluate = str2bool(args.evaluate)
args.learner = str2learner(args.learner)
args.outgate = str2bool(args.outgate)
args.random = str2bool(args.random)
args.data2 = str2bool(args.data2)
args.type = "MAX"

params = args

prefix = "../datasets_tokenized2/"
farr = []

farr = ["FNWN2013",  #0
        "MSRpar2012",  #2
        "MSRvid2012",  #3
        "OnWN2012",  #4
        "OnWN2013",  #5
        "OnWN2014",  #6
        "SMT2013",  #7
        "SMTeuro2012",  #8
        "SMTnews2012",  #9
        "answer-forum2015",  #12
        "answer-student2015",  #13
        "belief2015",  #14
        "deft-forum2014",  #18
        "deft-news2014",  #19
        "headline2013",  #20
        "headline2014",  #21
        "headline2015",  #22
        "images2014",  #23
        "images2015",  #24
        "tweet-news2014",
        "headlines2016",  # 28
        "plagarism2016",  # 29
        "postediting2016",  # 30
        "question2016",  # 31
        "answer2016"  # 32
        ]#27

data = []
train_d = []
dev_d = []
test_d = []
params.minval = 1
params.maxval = 6
for i in farr:
    d = get_data2(prefix + i)
    idx1 = int(len(d)*0.7)
    idx2 = int(len(d)*0.1)
    data.extend(d)
    train_d.extend(d[0:idx1])
    dev_d.extend(d[idx1:idx1+idx2])
    test_d.extend(d[idx1+idx2::])

print len(train_d)
print len(dev_d)
print len(test_d)

#print "Num examples:", len(data)
#sys.exit(0)

def getWordmap(textfile):
    words={}
    We = []
    f = io.open(textfile, 'r', encoding='utf-8')
    lines = f.readlines()
    if len(lines[0].split()) == 2:
        lines.pop(0)
    ct = 0
    for (n,i) in enumerate(lines):
        i=i.split()
        #if len(i) != 101:
        #    print "SKIPPED"
        #    continue
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=ct
        ct += 1
        #print i[0], n
        We.append(v)
    return (words, np.array(We))

def get_ngrams(examples, type=3):
    features = set([])
    for i in examples:
        for k in range(2):
            ln = i[k].phrase
            word = " "+ln.strip()
            for j in range(len(word)):
                idx = j
                gr = ""
                while idx < j + type and idx < len(word):
                    gr += word[idx]
                    idx += 1
                if not len(gr) == type:
                    continue
                features.add(gr)
    We = lasagne.init.Normal()
    We = We.sample((len(features) + 1, params.dim))
    words = {}
    for i in features:
        words[i] = len(words)
    words["UUUNKKK"] = len(words)
    return words, We

def get_words(examples):
    features = defaultdict(int)
    for i in examples:
        for k in range(2):
            ln = i[k].phrase
            word = ln.strip().split()
            for j in range(len(word)):
                features[word[j]] += 1
    features = sorted(features.items(), key = lambda x: x[1], reverse = True)
    if len(features) > 200000:
        features = features[0:200000]
    We = lasagne.init.Normal()
    We = We.sample((len(features) + 1, params.dim))
    words = {}
    for i in features:
        words[i[0]] = len(words)
    words["UUUNKKK"] = len(words)
    return words, We

if params.wordtype == "words":
    if params.random:
        words, We = get_words(data)
    else:
        words, We = getWordmap('../data/sl999_czeng3.txt')
else:
    words, We = get_ngrams(data)

params.vocab = words
model = bag_nmt_model(We, params)

print " ".join(sys.argv)
print "Num examples:", len(data)
print "Num words:", len(words)

model.train(train_d, dev_d, test_d, words, params)
