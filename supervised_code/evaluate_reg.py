from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import numpy as np

def getSeqs(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    return X1, X2

def getScore(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    seqs = [X1]
    x1,m1 = model.prepare_data(seqs)
    seqs = [X2]
    x2,m2 = model.prepare_data(seqs)
    scores = model.scoring_function(x1,x2,m1,m2)
    scores = np.squeeze(scores)
    return float(scores)

def getCorrelation(model,words,data,tri=False):
    preds = []
    golds = []
    seq1 = []
    seq2 = []
    ct = 0
    for i in data:
        X1, X2 = getSeqs(i[0].phrase,i[1].phrase,model,words)
        score = i[2]
        seq1.append(X1)
        seq2.append(X2)
        ct += 1
        if ct % 100 == 0:
            x1,m1 = model.prepare_data(seq1)
            x2,m2 = model.prepare_data(seq2)
            scores = model.scoring_function(x1,x2,m1,m2)
            scores = np.squeeze(scores)
            preds.extend(scores.tolist())
            seq1 = []
            seq2 = []
        golds.append(score)
    if len(seq1) > 0:
        x1,m1 = model.prepare_data(seq1)
        x2,m2 = model.prepare_data(seq2)
        scores = model.scoring_function(x1,x2,m1,m2)
        scores = np.squeeze(scores)
        preds.extend(scores.tolist())
    return pearsonr(preds,golds)[0], spearmanr(preds,golds)[0]

def evaluate(model,words,test,dev):
    p_test, _ = getCorrelation(model,words,test)
    p_dev, _ = getCorrelation(model,words,dev)
    print "{0} train {1} dev ".format(p_test, p_dev)
