import lasagne
import theano
import numpy as np
import theano.tensor as T
import utils

class gated_averaging_layer(lasagne.layers.MergeLayer):

    def __init__(self, incoming, num_units, mask_input=None, model_type=1, **kwargs):

        incomings = [incoming]
        self.mask_incoming_index = -1

        if mask_input is not None:
            incomings.append(mask_input)
            self.mask_incoming_index = len(incomings)-1

        self.num_units = num_units
        self.model_type = model_type

        super(gated_averaging_layer, self).__init__(incomings, **kwargs)
        input_shape = self.input_shapes[0]
        num_inputs = np.prod(input_shape[2:])

        self.Wx1 = self.add_param(lasagne.init.Normal(0.1), (num_inputs, num_units),
                                   name="Wx1")
        self.Wx2 = self.add_param(lasagne.init.Normal(0.1), (num_inputs, num_units),
                                   name="Wx2")
        self.bx = self.add_param(lasagne.init.Constant(0.), (num_units,),
                                   name="bx")

        self.Wh1 = self.add_param(lasagne.init.Normal(0.1), (num_inputs, num_units),
                                   name="Wh1")
        self.Wh2 = self.add_param(lasagne.init.Normal(0.1), (num_inputs, num_units),
                                   name="Wh2")
        self.bh = self.add_param(lasagne.init.Constant(0.), (num_units,),
                                   name="bh")

    def get_output_shape_for(self, input_shapes):
        input_shape = input_shapes[0]
        return input_shape[0], input_shape[1], self.num_units

    def get_output_for(self, inputs, **kwargs):

        input = inputs[0]
        mask = None

        if self.mask_incoming_index > 0:
            mask = inputs[self.mask_incoming_index]

        input = input.dimshuffle(1, 0, 2)
        seq_len, num_batch, _ = input.shape

        def step(xt, htm1, *args):

            if self.model_type == 1:
                sigma1 = lasagne.nonlinearities.sigmoid(T.dot(xt,self.Wx1) + T.dot(htm1,self.Wx2) + self.bx)
                sigma2 = lasagne.nonlinearities.sigmoid(T.dot(xt,self.Wh1) + T.dot(htm1,self.Wh2) + self.bh)
                return sigma1 * xt + sigma2 * htm1
            elif self.model_type == 2:
                sigma1 = lasagne.nonlinearities.sigmoid(T.dot(xt,self.Wx1) + T.dot(htm1,self.Wx2) + self.bx)
                return xt + sigma1 * htm1
            elif self.model_type == 3:
                sigma1 = lasagne.nonlinearities.sigmoid(T.dot(xt,self.Wx1) + T.dot(htm1,self.Wx2) + self.bx)
                return sigma1 * xt + htm1
            elif self.model_type == 4:
                sigma1 = lasagne.nonlinearities.sigmoid(T.dot(xt,self.Wx1) + T.dot(htm1,self.Wx2) + self.bx)
                return (1 - sigma1) * xt + sigma1 * htm1
            elif self.model_type == 5:
                return xt + htm1

        def step_masked(input_n, mask_n, htm1, *args):
            ht = step(input_n, htm1, *args)
            ht = T.switch(mask_n, ht, htm1)
            return ht

        if mask is not None:
            mask = mask.dimshuffle(1, 0, 'x')
            sequences = [input, mask]
            step_fun = step_masked
        else:
            sequences = input
            step_fun = step

        h_init = T.ones((num_batch, self.num_units))

        if self.model_type == 1:
            non_seqs = [self.Wx1, self.Wx2, self.bx, self.Wh1, self.Wh2, self.bh]
        else:
            non_seqs = [self.Wx1, self.Wx2, self.bx]

        hid_out = theano.scan(
            fn=step_fun,
            sequences=sequences,
            outputs_info=[h_init],
            go_backwards=False,
            non_sequences=non_seqs,
            strict=True)[0]

        hid_out = hid_out.dimshuffle(1, 0, 2)

        return hid_out
