
import lasagne
from theano import tensor as T

class lasagne_dot_layer(lasagne.layers.Layer):

    def __init__(self, incoming, num_units, num_layers = 1, W=lasagne.init.GlorotUniform(),
                 b=lasagne.init.Constant(0.), nonlinearity=lasagne.nonlinearities.identity,
                 **kwargs):
        super(lasagne_dot_layer, self).__init__(incoming, **kwargs)
        self.nonlinearity = nonlinearity

        self.num_units = num_units
        self.num_layers = num_layers

        num_inputs = self.input_shape[-1]

        self.W = self.add_param(W, (num_inputs, num_units), name="W_dot")
        self.b = self.add_param(b, (num_units,), name="b_dot",
                                    regularizable=False)
        if self.num_layers == 2:
            self.W2 = self.add_param(W, (num_units, num_units), name="W2_dot")
            self.b2 = self.add_param(b, (num_units,), name="b2_dot",
                                    regularizable=False)


    def get_output_shape_for(self, input_shape):
        return (input_shape[0], input_shape[1], self.num_units)

    def get_output_for(self, input, **kwargs):
        activation = T.dot(input, self.W)
        activation = activation + self.b.dimshuffle('x', 0)
        activation = self.nonlinearity(activation)
        if self.num_layers == 2:
            activation = T.dot(activation, self.W2)
            activation = activation + self.b2.dimshuffle('x', 0)
            activation = self.nonlinearity(activation)
        return activation
