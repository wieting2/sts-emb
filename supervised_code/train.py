from params import params
from lstm_ppdb_model2 import lstm_ppdb_model
import lasagne
import random
import numpy as np
import sys
import argparse
import pdb
from tree import tree
import utils
import cPickle

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

def str2act(v):
    if v is None:
        return v
    if v.lower() == "tanh":
        return lasagne.nonlinearities.tanh
    if v.lower() == "linear":
        return lasagne.nonlinearities.linear
    if v.lower() == "relu":
        return lasagne.nonlinearities.rectify
    raise ValueError('A type that was supposed to be a learner is not.')

def get_ngrams(data, params):
    ngrams = {}
    features = {}
    for i in data:
        lis1 = utils.get_ngrams(i[0], 3)
        lis2 = utils.get_ngrams(i[1], 3)
        lis = lis1 + lis2
        for i in lis:
            if i in ngrams:
                ngrams[i] += 1
            else:
                ngrams[i] = 1
    ngrams = sorted(ngrams.items(), key = lambda x: x[1], reverse=True)
    for i in ngrams:
        if i[1] >= params.cutoff:
            features[i[0]] = len(features)
    return features

def get_data2(f):
    f = open(f,'r')
    lines = f.readlines()
    #lines = lines[0:int(len(lines)*0.5)]
    data = []
    for i in lines:
        i = i.split("\t")
        p1 = i[0].strip(); p2 = i[1].strip(); score = float(i[2])
        score = score + 1
        #score = -1 + score * .4
        data.append((tree(p1), tree(p2), score))
    return data

def get_data3():
    def get_data(f):
        lines = f.readlines()
        data = []
        for i in lines:
            i = i.split("\t")
            p1 = i[0].strip(); p2 = i[1].strip(); score = float(i[2])
            #score = -1.5 + score * .5
            data.append((tree(p1), tree(p2), score))
        return data
    f = open('../datasets_tokenized/sicktrain','r')
    train = get_data(f)
    f = open('../datasets_tokenized/sickdev','r')
    dev = get_data(f)
    f = open('../datasets_tokenized/sicktest','r')
    test = get_data(f)
    return train, dev, test

random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LC", help="Regularization on Composition Parameters", type=float)
parser.add_argument("-LW", help="Regularization on Embedding Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.")
parser.add_argument("-dataf", help="Training data file.")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.")
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-fraction", help="Percent of training examples to use.", type=float)
parser.add_argument("-clip", help="float to indicate grad cutting. Use 0 for no clipping.",type=int)
parser.add_argument("-eta", help="learning rate",type=float)
parser.add_argument("-learner", help="Either AdaGrad or Adam")
parser.add_argument("-featurefile", help="File containing n-grams and their counts.")
parser.add_argument("-featurefileWord", help="File containing n-grams and their counts.")
parser.add_argument("-featurefilePhrase", help="File containing n-grams and their counts.")
parser.add_argument("-cutoff", help="Above or equal to this, features are kept.", type=int)
parser.add_argument("-offset", help="n in n-gram for features.", type=int)
parser.add_argument("-layers", help="Number of layers <= 2.", type=int)
parser.add_argument("-act1", help="activation of last layer of DSSM.")
parser.add_argument("-win", help="Number of words to include in context in the left and the right.", type=int)
parser.add_argument("-character_file", help="List of characters for embeddings.")
parser.add_argument("-outgate", help="Outgate")
parser.add_argument("-dropout", help="dropout", type=float)
parser.add_argument("-conv_type", help="conv_type", type=int)
parser.add_argument("-actconv")
parser.add_argument('-noshuffle')
parser.add_argument('-sum')
parser.add_argument("-model", help="", type=int)
parser.add_argument("-ngramtype", help="", type=int)
parser.add_argument("-charmode", help="")
parser.add_argument("-ls", help="", type=int)
parser.add_argument("-mode", help="")
parser.add_argument("-average", help="")
parser.add_argument("-lam", help="", type=float)
parser.add_argument("-alpha", help="", type=float)
parser.add_argument("-beta", help="", type=float)
parser.add_argument("-dim", help="", type=int)
parser.add_argument("-scramble",type=float)
parser.add_argument("-dropout1",type=float)
parser.add_argument("-dropout2",type=float)
parser.add_argument("-tri")
parser.add_argument("-prebatch")
parser.add_argument("-scramble_single")
parser.add_argument("-partial")
parser.add_argument("-model_type",type=int)
parser.add_argument("-sumlayer")
parser.add_argument("-shuffle_exp")
parser.add_argument("-data_type")
parser.add_argument("-loadfile")
parser.add_argument("-minval", type=int)
parser.add_argument("-maxval", type=int)

args = parser.parse_args()

params.LC = args.LC
params.LW = args.LW
params.outfile = args.outfile
params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.wordfile = args.wordfile
params.nntype = args.nntype
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.dataf = args.dataf
params.margin = args.margin
params.type = args.samplingtype
params.epochs = args.epochs
params.evaluate = str2bool(args.evaluate)
params.learner = str2learner(args.learner)
params.eta = args.eta
params.featurefile = args.featurefile
params.featurefileWord = args.featurefileWord
params.featurefilePhrase = args.featurefilePhrase
params.cutoff = args.cutoff
params.offset = args.offset
params.numlayers = args.layers
params.act = str2act(args.act1)
params.nntype = args.nntype
params.win = args.win
params.outgate = str2bool(args.outgate)
params.character_file = args.character_file
params.conv_type = args.conv_type
params.dropout = args.dropout
params.clip = args.clip
params.actconv = str2act(args.actconv)
params.noshuffle = str2bool(args.noshuffle)
params.sum = str2bool(args.sum)
params.model = args.model
params.nntype = args.nntype
params.ngramtype = args.ngramtype
params.charmode = args.charmode
params.ls = args.ls
params.mode = args.mode
params.average = str2bool(args.average)
params.lam = args.lam
params.alpha = args.alpha
params.beta = args.beta
params.dim = args.dim
params.scramble = args.scramble
params.dropout1 = args.dropout1
params.dropout2 = args.dropout2
params.tri = str2bool(args.tri)
params.prebatch = str2bool(args.prebatch)
params.scramble_single = str2bool(args.scramble_single)
params.partial = str2bool(args.partial)
params.model_type = args.model_type
params.sumlayer = str2bool(args.sumlayer)
params.shuffle_exp = str2bool(args.shuffle_exp)
params.data_type = args.data_type
params.loadfile = args.loadfile

prefix = "../datasets_tokenized/"
farr = []

farr = ["FNWN2013",#0
        "MSRpar2012",#2
        "MSRvid2012",#3
        "OnWN2012",#4
        "OnWN2013",#5
        "OnWN2014",#6
        "SMT2013",#7
        "SMTeuro2012",#8
        "SMTnews2012",#9
        "answer-forum2015",#12
        "answer-student2015",#13
        "belief2015",#14
        "deft-forum2014",#18
        "deft-news2014",#19
        "headline2013",#20
        "headline2014",#21
        "headline2015",#22
        "images2014",#23
        "images2015",#24
        "tweet-news2014"]#27


data = []
train_d = []
dev_d = []
test_d = []
if params.data_type == "sick":
    params.minval = 1
    params.maxval = 5
    train_d, dev_d, test_d = get_data3()
else:
    params.minval = 1
    params.maxval = 6
    for i in farr:
        d = get_data2(prefix + i)
        idx1 = int(len(d)*0.4)
        idx2 = int(len(d)*0.1)
        data.extend(d)
        train_d.extend(d[0:idx1])
        dev_d.extend(d[idx1:idx1+idx2])
        test_d.extend(d[idx1+idx2::])

print " ".join(sys.argv)
print "Num examples:", len(train_d), len(dev_d), len(test_d)

model = lstm_ppdb_model(We, params)

model.train(train_d, dev_d, test_d, words, params)
