mkdir data
cd data

#download pre-processed data, ppdb datasets, word embeddings, etc.
wget http://ttic.uchicago.edu/~wieting/backtrans-demo.zip
unzip -j millions-demo.zip
rm millions-demo.zip