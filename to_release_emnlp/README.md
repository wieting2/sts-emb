# emnlp17

Code to train models from "Learning Paraphrastic Sentence Embeddings from Back-Translated Bitext".

The code is written in python and requires numpy, scipy, nltk, theano, and the lasagne libraries.

To get started, run setup.sh to download the required files such as training data and evaluation data. There is a demo script that takes the model that you would like to train as a command line argument (check the script to see available choices). Check main/train.py for command line options.

If you use our code for your work please cite:

@inproceedings{wieting-17-backtrans,
        author = {John Wieting, Jonathan Mallinson, and Kevin Gimpel},
        title = {Learning Paraphrastic Sentence Embeddings from Back-Translated Bitext},
        booktitle = {Proceedings of Empirical Methods in Natural Language Processing},
        year = {2017}
}
