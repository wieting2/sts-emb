import utils
from translation_quality_model import translation_quality_model
import lasagne
import random
import numpy as np
import sys
import argparse
import io
from example import example

def get_data(params):
    lines = io.open(params.data, 'r', encoding='utf-8').readlines()

    d = {}
    data = []

    for i in lines:
        i = i.split('\t')
        t = i[1]
        r = i[0]
        idx = int(i[-2])
        if idx in d:
            d[idx] = (d[idx][0], d[idx][1] + [t])
        else:
            d[idx] = (r,[t])

    for i in d.items():
        r = i[1][0]
        trans = i[1][1]
        r = example(r)
        trans = [example(j) for j in trans]
        data.append((i[0], r, trans))

    train = []; test = []; val = []
    for i in data:
        idx, r, t = i
        if idx < 40000:
            train.append((i[1],i[2]))
        elif idx < 45000:
            val.append((i[1],i[2]))
        elif idx < 50000:
            test.append((i[1],i[2]))

    return train, val, test

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

random.seed(1)
np.random.seed(1)

parser = argparse.ArgumentParser()

parser.add_argument("-LC", help="Regularization on composition parameters", type=float, default=0.)
parser.add_argument("-outfile", help="Name of output file")
parser.add_argument("-batchsize", help="Size of batch", type=int, default=100)
parser.add_argument("-dim", help="Dimension of model", type=int, default=300)
parser.add_argument("-wordfile", help="Word embedding file", default="../data/sl999_XXL_data.txt")
parser.add_argument("-save", help="Whether to pickle model", type=int, default=0)
parser.add_argument("-evaluate", help="Whether to evaluate the model during training", type=int, default=1)
parser.add_argument("-epochs", help="Number of epochs in training", type=int, default=10)
parser.add_argument("-eta", help="Learning rate", type=float, default=0.001)
parser.add_argument("-learner", help="Either AdaGrad or Adam", default="adam")
parser.add_argument("-model", help="Which model to use between (bi)lstm or wordaverage")
parser.add_argument("-data", help="Name of data file containing paraphrases", default=None)
parser.add_argument("-bilstm_combination", help="Approach to merge forward and "
                                                       "backward hidden states. Either ADD or MLP", default = "ADD")

args = parser.parse_args()
args.learner = str2learner(args.learner)

params = args

train_data, val_data, test_data = get_data(params)
(words, We) = utils.get_wordmap(params.wordfile)

model = translation_quality_model(We, params)

print " ".join(sys.argv)
print "Num examples:", len(train_data)

train_eval = train_data[0:5000]

model.train(train_data, val_data, test_data, train_eval, words, params)