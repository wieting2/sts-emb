#mkdir data
#cd data

#download data, word embeddings, etc.
#wget http://ttic.uchicago.edu/~wieting/backtrans-demo.zip
#unzip -j backtrans-demo.zip
#rm backtrans-demo.zip

#filter some data
cd main
python filter_data.py -filtering_method trans -infile ../data/giga.fr -outfile ../data/giga.fr.trans
python filter_data.py -filtering_method length -infile ../data/giga.fr -outfile ../data/giga.fr.len
python filter_data.py -filtering_method ovl-1 -infile ../data/giga.fr -outfile ../data/giga.fr.ovl1
python filter_data.py -filtering_method ovl-2 -infile ../data/giga.fr -outfile ../data/giga.fr.ovl2
python filter_data.py -filtering_method ovl-3 -infile ../data/giga.fr -outfile ../data/giga.fr.ovl3