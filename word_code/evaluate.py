from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from utils import lookupIDX
import utils
from tree import tree
import sys
sys.path.insert(0,'../sentence_code')
from learn_bpe import learn
import apply_bpe
import numpy as np

cache = {}

def getwordsim(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        i=i.lower()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[2]))
            examples.append(ex)
    return examples

def getsimlex(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        i=i.lower()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[3]))
            examples.append(ex)
    return examples

def getSeqs(p1,p2,words,params,bpe,bpe_vocab,ovl):
    size = None

    if p1 in cache and p2 in cache:
        return cache[p1].embeddings, cache[p2].embeddings

    _p1 = p1
    _p2 = p2

    p1 = tree(p1)
    p2 = tree(p2)

    if params.wordtype == "2grams":
        size = 2
    elif params.wordtype == "3grams":
        size = 3
    elif params.wordtype == "4grams":
        size = 4
    elif params.wordtype == "5grams":
        size = 5

    if params.wordtype == "words":
        p1.populate_embeddings(words, True)
        p2.populate_embeddings(words, True)
    elif params.wordtype == "bpe":
        if params.bpe_file:
            if params.word_embedding_file:
                segment = bpe.segment(p1.phrase).strip().replace('@@', "")
                p1.phrase = segment
                segment = bpe.segment(p2.phrase).strip().replace('@@', "")
                p2.phrase = segment
            else:
                segment = bpe.segment(p1.phrase.replace(" ","_")).strip().replace('@@', "")
                p1.phrase = segment
                segment = bpe.segment(p2.phrase.replace(" ","_")).strip().replace('@@', "")
                p2.phrase = segment
        else:
            p1.phrase, p2.phrase = apply_bpe.seg(p1.phrase, p2.phrase, bpe, params, bpe_vocab)
        p1.populate_embeddings(words, params.use_unktoken)
        p2.populate_embeddings(words, params.use_unktoken)
    elif "gram" in params.wordtype and params.overlap:
        p1.populate_embeddings_ngrams(words, size, params.use_unktoken)
        p2.populate_embeddings_ngrams(words, size, params.use_unktoken)
    elif "gram" in params.wordtype and not params.overlap:
        p1.populate_embeddings_ngrams_nonverlap(words, size, params.use_unktoken)
        p2.populate_embeddings_ngrams_nonverlap(words, size, params.use_unktoken)

    cache[_p1]=p1
    cache[_p2]=p2

    return p1.embeddings, p2.embeddings

def getCorrelation(data,model,words,params,bpe,bpe_vocab):
    preds = []
    golds = []
    seq1 = []
    seq2 = []
    ct = 0
    for i in data:
        p1 = i[0]; p2 = i[1]; score = i[2]
        X1, X2 = getSeqs(p1,p2,words,params,bpe,bpe_vocab,params.bpe_overlap)
        #pdb.set_trace()
        seq1.append(X1)
        seq2.append(X2)
        ct += 1
        if ct % 100 == 0:
            #pdb.set_trace()
            x1,m1 = model.prepare_data(seq1)
            x2,m2 = model.prepare_data(seq2)
            scores = model.scoring_function(x1,x2,m1,m2)
            scores = np.squeeze(scores)
            preds.extend(scores.tolist())
            seq1 = []
            seq2 = []
        golds.append(score)
    if len(seq1) > 0:
        x1,m1 = model.prepare_data(seq1)
        x2,m2 = model.prepare_data(seq2)
        scores = model.scoring_function(x1,x2,m1,m2)
        scores = np.squeeze(scores)
        preds.extend(scores.tolist())
    return spearmanr(preds,golds)[0]

def evaluateWordSim(model,words,params,bpe,bpe_vocab):
    ws353ex = getwordsim('../datasets_tokenized/wordsim353.txt')
    ws353sim = getwordsim('../datasets_tokenized/wordsim-sim.txt')
    ws353rel = getwordsim('../datasets_tokenized/wordsim-rel.txt')
    simlex = getsimlex('../datasets_tokenized/SimLex-999.txt')
    rw = getwordsim('../datasets_tokenized/rw.txt')
    c1 = getCorrelation(ws353ex,model,words,params,bpe,bpe_vocab)
    c2 = getCorrelation(ws353sim,model,words,params,bpe,bpe_vocab)
    c3 = getCorrelation(ws353rel,model,words,params,bpe,bpe_vocab)
    c4 = getCorrelation(simlex,model,words,params,bpe,bpe_vocab)
    c5 = getCorrelation(rw,model,words,params,bpe,bpe_vocab)
    return [c1,c2,c3,c4,c5]

def evaluate_all(model,words,params,bpe,bpe_vocab):
    corr = evaluateWordSim(model,words,params,bpe,bpe_vocab)
    s="{0} {1} {2} {3} {4} ws353 ws-sim ws-rel sl999 rw".format(corr[0], corr[1], corr[2], corr[3], corr[4])
    print s
    return corr[3]
