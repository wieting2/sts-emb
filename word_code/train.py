from utils import getData
from paragram_model import paragram_model
import lasagne
import random
import numpy as np
import argparse
import sys
import pdb
sys.path.insert(0,'../sentence_code')
from learn_bpe import learn
from apply_bpe import apply
from apply_bpe import BPE
from apply_bpe2 import apply2
from theano import config

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

def str2act(v):
    if v is None:
        return v
    if v.lower() == "tanh":
        return lasagne.nonlinearities.tanh
    if v.lower() == "linear":
        return lasagne.nonlinearities.linear
    if v.lower() == "relu":
        return lasagne.nonlinearities.rectify
    raise ValueError('A type that was supposed to be a learner is not.')

def get_bpe(examples, params):

    bpe2 = None
    bpe_vocab = None

    if params.bpe_file:
        bpe2 = apply2(params.bpe_file, examples, params)
    else:
        bpe1 = learn(examples, params.bpe_size)
        bpe2 = BPE(bpe1, '@@', None, None)
        bpe_vocab = set([])
        for i in bpe2.bpe_codes: bpe_vocab.add("".join(i).replace("</w>", ""))
        apply(bpe1, examples, params, bpe_vocab)

    words = get_words(examples,0)

    return bpe2, words, bpe_vocab

def get_ngrams(examples, size, cutoff):
    d = {}
    for i in examples:
        e1 = " " + i[0].phrase + " "
        e2 = " " + i[1].phrase +" "
        for j in range(len(e1)):
            ngram = e1[j:j+size]
            if len(ngram) != size:
                continue
            if ngram in d:
                d[ngram] += 1
            else:
                d[ngram] = 1
        for j in range(len(e2)):
            ngram = e2[j:j+size]
            if len(ngram) != size:
                continue
            if ngram in d:
                d[ngram] += 1
            else:
                d[ngram] = 1
    feature_map = {}
    idx = 0
    for i in d:
        if d[i] > cutoff:
            feature_map[i] = idx
            idx += 1
    feature_map["UUUNKKK"] = idx
    return feature_map

def get_words(examples, cutoff):
    d = {}
    for i in examples:
        e1 = i[0].phrase.split()
        e2 = i[1].phrase.split()
        for j in range(len(e1)):
            wd = e1[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
        for j in range(len(e2)):
            wd = e2[j]
            if wd in d:
                d[wd] += 1
            else:
                d[wd] = 1
    feature_map = {}
    idx = 0
    #pdb.set_trace()
    for i in d:
        if d[i] > cutoff:
            feature_map[i] = idx
            idx += 1
    feature_map["UUUNKKK"] = idx
    return feature_map

random.seed(1)
np.random.seed(1)

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Embedding Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-dim", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-vocabfile", help="Word file to be read in.")
parser.add_argument("-save", help="Whether to pickle the model.", default="false")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.", default="MAX")
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.", default="True")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-eta", help="learning rate", type=float, default=0.001)
parser.add_argument("-learner", help="Either AdaGrad or Adam", default="Adam")
parser.add_argument("-outgate", help="Outgate")
parser.add_argument("-dropout", help="dropout", type=float)
parser.add_argument("-model", help="", type=int)
parser.add_argument("-mode", help="", default="sentence")
parser.add_argument("-model_type", type=int)
parser.add_argument("-max", type=int, default=0)
parser.add_argument("-word_embedding_file")
parser.add_argument("-bpe_file")
parser.add_argument("-bpe_size", type=int)
parser.add_argument("-bpe_overlap", type=int, default=-1)
parser.add_argument("-cutoff", type=int, default=0)
parser.add_argument("-wordtype", default="word", help = "words, 2grams, 3grams, 4grams, be, beoverlap, wp, wpoverlap")
parser.add_argument("-use_unktoken", default="true")
parser.add_argument("-overlap", default = "false")

args = parser.parse_args()

args.save = str2bool(args.save)
args.evaluate = str2bool(args.evaluate)
args.learner = str2learner(args.learner)
args.outgate = str2bool(args.outgate)
args.use_unktoken = str2bool(args.use_unktoken)
args.overlap = str2bool(args.overlap)

params = args

def getWordmap(wordfile, vocabfile):
    words={}
    We = []
    vocab = set([])
    f = open(vocabfile, 'r')
    lines = f.readlines()
    for i in lines:
        vocab.add(i.split()[0].strip())
    f = open(wordfile,'r')
    lines = f.readlines()
    idx = 0
    for (n,i) in enumerate(lines):
        i=i.split()
        if len(i) == 2:
            continue
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        if i[0] in vocab:
            words[i[0]]=idx
            We.append(v)
            idx += 1
    words["UUUNKKK"] = len(words)
    We = np.asarray(We, dtype=config.floatX)
    #m2 = lasagne.init.Uniform(1).sample((1, params.dim))
    #We = np.concatenate([We, m2])
    m2 = We.sum(axis=0) / We.shape[0]
    We = np.concatenate([We, m2.reshape(1, m2.size)])
    return We, words

#We, words = getWordmap(params.wordfile, params.vocabfile)
data = getData("../data/ppdb-XXL-ordered-lexical-data.txt")

bpe = None; bpe_vocab = None

We = None
if params.wordtype == "words":
    words = get_words(data, params.cutoff)
elif params.wordtype == "2grams":
    words = get_ngrams(data, 2, params.cutoff)
elif params.wordtype == "3grams":
    words = get_ngrams(data, 3, params.cutoff)
elif params.wordtype == "4grams":
    words = get_ngrams(data, 4, params.cutoff)
elif params.wordtype == "5grams":
    words = get_ngrams(data, 5, params.cutoff)
elif params.wordtype == "bpe":
    bpe, words, bpe_vocab = get_bpe(data, params)

params.vocab = words

#pdb.set_trace()

print " ".join(sys.argv)
print "Num examples:", len(data)
print "Num words:", len(words)

model = paragram_model(None, params)
model.train(data, words,params,bpe,bpe_vocab)
