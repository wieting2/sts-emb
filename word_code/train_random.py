from params import params
from tree import tree
from paragram_random_model import paragram_random_model
import lasagne
import random
import numpy as np
import argparse
import sys
import pdb

def getData(f):
    data = open(f,'r')
    lines = data.readlines()
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split('\t')
            if len(i) == 2:
                e = (tree(i[0]), tree(i[1]))
                examples.append(e)
            else:
                pass
                #print i
    return examples

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def str2learner(v):
    if v is None:
        return None
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

def str2act(v):
    if v is None:
        return v
    if v.lower() == "tanh":
        return lasagne.nonlinearities.tanh
    if v.lower() == "linear":
        return lasagne.nonlinearities.linear
    if v.lower() == "relu":
        return lasagne.nonlinearities.rectify
    raise ValueError('A type that was supposed to be a learner is not.')

random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Words", type=float, default=0.)
parser.add_argument("-LC", help="Regularization on NN paramss", type=float)
parser.add_argument("-outfile", help="Output file name", default = "paragram.words")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-dim", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.", default="False")
parser.add_argument("-dataf", help="Training data file.", default="../data/ppdb-XXL-ordered-lexical-data.txt")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.", default = "MAX")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.", default="True")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int, default = 10)
parser.add_argument("-fraction", help="Percent of training examples to use.", type=float)
parser.add_argument("-nntype", help="Either paragram or hash.")
parser.add_argument("-learner", help="Either AdaGrad or Adam", default = "Adam")
parser.add_argument("-featurefile", help="File containing n-grams and their counts.")
parser.add_argument("-cutoff", help="Above or equal to this, features are kept.", type=int, default=0)
parser.add_argument("-offset", help="n in n-gram for features.", type=int)
parser.add_argument("-numlayers", help="Above or equal to this, features are kept.", type=int)
parser.add_argument("-act", help="n in n-gram for features.")
parser.add_argument("-quality_cutoff", help="Cutoff on training_data", type=int)
parser.add_argument("-character_file", help="List of characters for embeddings.")
parser.add_argument("-outgate", help="Outgate")
parser.add_argument("-dropout", help="dropout", type=float)
parser.add_argument("-conv_type", help="conv_type", type=int)
parser.add_argument("-actconv")
parser.add_argument('-noshuffle')
parser.add_argument("-eta", help="learning rate", type=float, default=0.001)

args = parser.parse_args()

params.LW = args.LW
params.LC = args.LC
params.outfile = args.outfile
params.batchsize = args.batchsize
params.dim = args.dim
params.wordfile = args.wordfile
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.dataf = args.dataf
params.margin = args.margin
params.type = args.samplingtype
params.epochs = args.epochs
params.evaluate = str2bool(args.evaluate)
params.nntype = args.nntype
params.learner = str2learner(args.learner)
params.featurefile = args.featurefile
params.cutoff = args.cutoff
params.offset = args.offset
params.numlayers = args.numlayers
params.act = str2act(args.act)
params.outgate = str2bool(args.evaluate)
params.character_file = args.character_file
params.conv_type = args.conv_type
params.dropout = args.dropout
params.outfile = '../models/'+args.outfile
params.actconv = str2act(args.actconv)
params.noshuffle = str2bool(args.noshuffle)

examples = getData(params.dataf)

if args.fraction:
    examples = examples[0:int(round(args.fraction * len(examples)))]

print "No. of examples: ", len(examples)
print " ".join(sys.argv)
model = None

if args.learner and args.eta:
    params.learner = str2learner(args.learner)
    params.eta = args.eta
else:
    params.learner = lasagne.updates.adam
    params.eta = 0.001

words = {}
for i in examples:
    w1 = i[0].phrase
    w2 = i[1].phrase
    if w1 in words:
        words[w1] += 1
    else:
        words[w1] = 1

    if w2 in words:
        words[w2] += 1
    else:
        words[w2] = 1

words2 = {}
idx = 0
for i in words:
    if words[i] > 1:
        words2[i] = idx
        idx += 1

words = words2
words['UUUNKKK'] = idx

#for i in words:
#    print i +"\t" + str(words[i])

#model = paragram_random_model(words, params)
#model.train(examples,words,params)
