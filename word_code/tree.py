
import random
import numpy as np
import pdb

def lookup(words,w):
    w = w.lower()
    if w in words:
        return words[w]
    else:
        #print w
        return words['UUUNKKK']

def lookup_no_unk(words,w):
    w = w.lower()
    if w in words:
        return words[w]

class tree(object):

    def __init__(self, phrase):
        self.phrase = phrase.strip().lower()
        self.embeddings = []
        self.representation = None

    def populate_embeddings(self, words, unk):
        phrase = self.phrase.lower()
        arr = phrase.split()
        for i in arr:
            if unk:
                self.embeddings.append(lookup(words,i))
            else:
                w = lookup_no_unk(words, i)
                if w:
                    self.embeddings.append(w)

    def populate_embeddings_ngrams(self, words, size, unk):
        phrase = " " + self.phrase.lower() + " "
        for j in range(len(phrase)):
            ngram = phrase[j:j+size]
            if len(ngram) != size:
                continue
            if unk:
                self.embeddings.append(lookup(words, ngram))
            else:
                w = lookup_no_unk(words, ngram)
                if w:
                    self.embeddings.append(w)
        if len(self.embeddings) == 0:
            self.embeddings = [words['UUUNKKK']]

    def populate_embeddings_ngrams_nonverlap(self, words, size, unk):
        phrase = " " + self.phrase.lower() + " "
        if len(phrase) % size == 1:
            phrase = phrase + " "
        elif size > 2 and len(phrase) % size == 2:
            phrase = phrase + "  "
        elif size > 3 and len(phrase) % size == 3:
            phrase = phrase + "   "
        for j in range(0, len(phrase), size):
            ngram = phrase[j:j+size]
            if unk:
                self.embeddings.append(lookup(words, ngram))
            else:
                w = lookup_no_unk(words, ngram)
                if w:
                    self.embeddings.append(w)

    def unpopulate_embeddings(self):
        self.embeddings = []
